# **acne**

## Overview

Acne is a tool for the game ███████████ written in C++ and compiled as a dynamic link library that gets injected into the process.

This tool provides a set of functionalities for game interaction, manipulation and gameplay.
This project has been made after reverse engineering the game and analyzing how it works "behind the scenes", some features violate the game policies, I am not responsible for any repercussions caused by the use of this software.

## Features

- **Class Exposure**: Gain access to essential game classes, empowering users to manipulate and interact with game elements in new and creative ways.
- **Function Accessibility**: Unlock various game functions, enabling users to automate tasks, create custom scripts, and enhance gameplay according to their preferences.
- **Policy Compliance**:  The tool has been developed with strict adherence to [Game Name]'s policies and terms of service, ensuring a secure and fair gaming environment for all users. [^1]

[^1]: Code that does not comply has been redacted. The use of some functions MAY still break game policies.

## How to Use

To incorporate acne into your gaming experience, follow these simple steps:

1. **Download** the source and compile it.
2. **Sign the binaries** with your code signing certificate. [^2]
3. **Load the library** with whatever tool you prefer.
4. **Explore the project**, make your own custom scripts and have fun!

[^2]: Crucial step, injecting the library with no signing certificate will result in a game ban!

[Watch Demo Video](assets/demo.mp4)
