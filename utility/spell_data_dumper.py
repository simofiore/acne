import urllib.request
import re
import json


headers = {}
headers['User-Agent'] = "Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.17 (KHTML, like Gecko) Chrome/24.0.1312.27 Safari/537.17"
regex_pattern = '<a href="[\w\.]+/?" title="[\w\.]+">([\w\.]+)/?</a>'

file_names = [] 





def parse():

    final_json = {}

    for file_name in file_names:
        champion_name = file_name.split('.')[0].capitalize()
        with open(file_name) as text_file :

            json_obj = json.loads(text_file.read())
            for node in json_obj:
                if 'mSpell' in json_obj[node]:
                    final_json[champion_name] = {}
                    final_json[champion_name][node] = json_obj[node]
                    continue




def parsefile(file : str):
    with open(file) as text_file :
        obj = json.loads(text_file.read())
        test = 1



def retrieve_html_from_url(url : str) :
    global file_names

    fp = urllib.request.Request(url, headers= headers)
    mybytes = urllib.request.urlopen(fp).read()
    mystr = mybytes.decode("utf8")

    regex_result = re.findall(regex_pattern, mystr)


    for item in regex_result :
        url_request = 'https://raw.communitydragon.org/latest/game/data/characters/{}/{}.bin.json'.format(item,item)
        request = urllib.request.Request(url_request, headers= headers)
        try:
            request_bytes = urllib.request.urlopen(request).read()
        except: 
            continue
        request_string = request_bytes.decode("utf8")

        json_obj = json.loads(request_string)
        try:
            if(json_obj['Characters/{}/CharacterRecords/Root'.format(item.capitalize())]['unitTagsString'] == 'Champion'):
                file_name = '{}.bin.json'.format(item)
                file_names.append(file_name)

                with open(file_name, "w") as text_file :
                    text_file.write(request_string)
                return
        except:
            continue
              

            






retrieve_html_from_url("http://raw.communitydragon.org/latest/game/data/characters/")
parse()