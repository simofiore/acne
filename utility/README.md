## **UTILITY**

- *champ_icon_dumper.py*:   downloads champion icons and saves them as a byte array in a header, it then creates a map with the champion name as key.
- *spell_data_dumper.py*:   downloads information about champion spells and saves them in a .json file.
- *offset_dumper.py*:   given a list of signatures, finds the offset from a clean game dump using IDA.
