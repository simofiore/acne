from idc import BADADDR, SEARCH_DOWN
import idc
import idaapi
import datetime


def MakeNamespace(enumName, offsetArray):
    print("namespace %s {" % enumName)
    for offset in offsetArray:
        if len(offset[0]) == 0:
            print("")
            continue
        if type(offset[1]) is str:
            print("   constexpr std::uint32_t %s = %s;" % (offset[0], offset[1]))
            continue
        fncValue = offset[1] if offset[1] != -1 else 0x0
        isMismatch = fncValue == BADADDR or fncValue == 0
        if (fncValue >= idaapi.get_imagebase() and not isMismatch):
            idc.set_name(fncValue, offset[0], 0x00 | 0x02 | 0x20)
            fncValue = fncValue - idaapi.get_imagebase()
        print("   constexpr std::uint32_t %s{ 0x%X }; %s" % (offset[0], fncValue, "// broken pattern" if isMismatch else ""))
    print("};\r")


def FindOffsetByString(name, offset, operandValue):
    address = idc.find_binary(0, SEARCH_DOWN, "\"" + name + "\"")
    dword = -1
    if address == BADADDR:
        return BADADDR
    xrefs = XrefsTo(address)
    for xref in xrefs:
        dword = xref.frm + offset
    if dword == BADADDR:
        return BADADDR
    return idc.get_operand_value(dword, operandValue)


def FindFuncCall(Pattern):
    addr = idc.find_binary(0, SEARCH_DOWN, Pattern)
    if addr == BADADDR: return 0
    return idc.get_operand_value(addr, 0)


def FindFunctionByPatternStartEA(pattern):
	address = idc.find_binary(0, SEARCH_DOWN, pattern)
	if address == BADADDR:
		return BADADDR
	try:
		return idaapi.get_func(address).start_ea
	except Exception:
		return -1


def FindOffsetPattern(Pattern, Operand):
    addr = idc.find_binary(0, SEARCH_DOWN, Pattern)
    if addr == BADADDR: return 0
    return idc.get_operand_value(addr, Operand)


def FindFunctionFirstXRef(name):
	address = idc.find_binary(0, SEARCH_DOWN, "\"" + name + "\"")
	dword = BADADDR
	if address == BADADDR:
		return BADADDR
	xrefs = XrefsTo(address)
	for xref in xrefs:
		dword = xref.frm
	try:
		return idaapi.get_func(dword).start_ea
	except Exception:
		return -1


def main():
    print("// [*] League of Legends Update Tool")
    print("// [*] By R3nzTheCodeGOD")
    print("// [*] Started at: %s" % datetime.datetime.now())
    MakeNamespace("AiBaseClient", [
          ["AiBaseClient::IssueOrder", FindFuncCall("E8 ?? ?? ?? ?? 8D 43 11 75")]
    ])
    MakeNamespace("NavigationMesh",[
          ["NavigationMesh::BuildNavigationPath" , FindFunctionByPatternStartEA("48 8B C4 48 89 58 10 55 56 57 41 54 41 55 41 56 41 57 48 8D")]
    ])



if __name__ == "__main__":
    main()