import urllib.request
import re

headers = {}
headers['User-Agent'] = "Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.17 (KHTML, like Gecko) Chrome/24.0.1312.27 Safari/537.17"
regex_pattern = '<a href="[\w\.]+/?" title="[\w\.]+">([\w\.]+)/?</a>'
custom_filenames = {
    "ahri": "ahri_circle_0.skins_ahri_asu_prepro.png",
    "anivia": "cryophoenix_circle.png",
    "belveth": "belveth_circle_0.belveth.png",
    "blitzcrank": "steamgolem_circle.png",
    "hecarim": "hecarim_circle_0.pie_c_13_6.png",
    "ksante": "ksante_circle_0.ksante.png",
    "milio": "milio_circle_0.milio.png",
    "shaco": "jester_circle.png",
    "udyr": "udyr_circle_0.udyrvgu.png",
    "zilean": "chronokeeper_circle.png",
    "orianna": "oriana_circle.png",
    "nilah": "nilah_circle_0.nilah.png",
    "chogath": "greenterror_circle.png",
    "rammus": "armordillo_circle.png",
}
map_members = []

final_file = open("E:\\Repos MIND CONTROL 13.10\\acne-league\\Icons.h", "w")

blacklist = ["apheliosturret","azirsundisc", "gnarbig","kingporo","sennasoul", "viegosoul", "tft", "sru", "nexusblitz", "preseason_turret_shield", "slime", "graspingplant", "thornplant"]

def make_map_entry(champ : str, size ):
    global map_members
    entry = "{{\"{}\", {{{},{}}}}}".format(champ.capitalize(), "(void*){}".format(champ), size)
    map_members.append(entry)

def make_c_variable(champ : str, bytes : bytearray):
    formatted_bytes = ', '.join('0x{:02X}'.format(byte) for byte in bytes)
    cpp_variable = f"static const BYTE {champ}[{len(bytes)}] = {{ {formatted_bytes} }};"
    final_file.write(cpp_variable)
    final_file.write("\n")

def dump_map():
    final_file.write("\nstatic inline std::unordered_map<std::string, Structure> Map = {\n")
    for member in map_members:
        final_file.write(member)
        final_file.write(",\n")
    final_file.write("};\n\n")
     
     

def generate_from(url : str):
    fp = urllib.request.Request(url, headers= headers)
    mybytes = urllib.request.urlopen(fp).read()
    mystr = mybytes.decode("utf8")
    hero_name_array = re.findall(regex_pattern, mystr)

    for hero_name in hero_name_array:

        blacklisted = False
        for word in blacklist:
            if word in hero_name:
                blacklisted = True
                break

        if blacklisted:
            continue

        endpoint = "https://raw.communitydragon.org/latest/game/assets/characters/{}/hud/{}{}".format(hero_name, hero_name, "_circle.png")
        try:
            fp = urllib.request.Request(endpoint, headers= headers)
            image_data = urllib.request.urlopen(fp).read()
            byte_array = bytearray(image_data)
            make_map_entry(hero_name, len(byte_array))
            make_c_variable(hero_name, byte_array)
            continue
        except:
            pass
        
        endpoint = "https://raw.communitydragon.org/latest/game/assets/characters/{}/hud/{}{}".format(hero_name, hero_name, "_circle_0.png")
        try:
            fp = urllib.request.Request(endpoint, headers= headers)
            image_data = urllib.request.urlopen(fp).read()
            byte_array = bytearray(image_data)
            make_map_entry(hero_name, len(byte_array))
            make_c_variable(hero_name, byte_array)
            continue
        except:
            pass


        try:
            custom_file = custom_filenames[hero_name]
            endpoint = "https://raw.communitydragon.org/latest/game/assets/characters/{}/hud/{}".format(hero_name, custom_file)
            fp = urllib.request.Request(endpoint, headers= headers)
            image_data = urllib.request.urlopen(fp).read()
            byte_array = bytearray(image_data)
            make_map_entry(hero_name, len(byte_array))
            make_c_variable(hero_name, byte_array)
            continue
        except:
            pass



        # print(endpoint)
        # try:
        # #request = urllib.request.urlopen(fp)
        
        #     print("{} , request = {}".format(hero_name, fp.getcode()))
        # except:
        #     print("ERROR: couldnt access site for object {}".format(hero_name))




        # try:
        #     custom_file = custom_filenames[hero_name]
        #     endpoint = "https://raw.communitydragon.org/latest/game/assets/characters/{}/hud/{}".format(hero_name, custom_file)
        #     download_from_url(hero_name, endpoint)
        # except:
        #     continue



def main():
    final_file.write("#pragma once\n\nnamespace Icons{\n\nstruct Structure{\nvoid* icon;\nint size;\n};\n\n")
    generate_from("https://raw.communitydragon.org/latest/game/assets/characters/")
    dump_map()
    final_file.write("}\n\n")


main()