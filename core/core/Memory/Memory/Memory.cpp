#include "Memory.h"
#include "Memory/Utility/find_hidden_module.h"

void Memory::Init() {
	_main = (QWORD)GetModuleHandle(NULL);
	if (!_main) print("WARNING: NO MAIN MODULE FOUND");

	_hidden = GetShadowHandle();
	if (!_main) print("WARNING: NO SHADOW MODULE FOUND");

	_stub = (QWORD)GetModuleHandle("stub.dll");
	if (!_main) print("WARNING: NO STUB MODULE FOUND");

	print("main: 0x%p - shadow: 0x%p - stub: 0x%p", _main, _hidden, _stub);
}
