#pragma once

class Whore
{

	int _enabled;
	int _bytes_to_copy;
	int _OVERRIDE;
	unsigned _original_buffer[32];

	void* _original_league_function;
	void* _original_shadow_function;
	void* _callback_function;
	void* _trampoline;


public:

	static inline std::map<QWORD, BYTE> patched_bytes;


	Whore(QWORD original, void* callback, int size, bool is_stub = false);
	Whore();
	void Enable();

	void* GetTrampoline() { return _trampoline; };
};

