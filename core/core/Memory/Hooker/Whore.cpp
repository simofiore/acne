#include "Whore.h"

/*

	For every game function, copy the original first 14 bytes (that are replaced by the jmp to our callback)
	into an allocated executable memory.

*/

static constexpr unsigned char JUMP_CODE[14] = { 0xff, 0x25, 0x00, 0x00, 0x00, 0x00, 0x99, 0x99, 0x99, 0x99, 0x99, 0x99, 0x99, 0x99 };

#define CREATE_JUMP(name, targetAddress) \
	unsigned char name[sizeof(JUMP_CODE)]; \
	memcpy(name, (unsigned char*)JUMP_CODE, sizeof(JUMP_CODE)); \
	*(unsigned long long*)((QWORD)name + 6) = (unsigned long long)targetAddress


BYTE fixed_render_gui[]{

	0x48, 0x89, 0x6c, 0x24, 0x18,
	0x56,
	0x48, 0x83, 0xec, 0x20,
	0x48, 0xb8, 0x99, 0x99, 0x99, 0x99, 0x99, 0x99, 0x99, 0x99

	//0x51,
	//0x48, 0xb9, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // move into rcx 8 bytes, cmp or do shit and then restore rcx
	//0x59

};

// Minimum size is 14
Whore::Whore(QWORD original, void* callback, int size, bool is_stub) {

	// FIX ADDRESSES FOR HIDDEN MODULE OR STUB.DLL
	if (!is_stub) {
		_original_league_function = (void*)RVA(original);
		_original_shadow_function = (void*)RVA_H(original);
	}
	else {
		_original_league_function = (void*)RVA_S(original);
		_original_shadow_function = (void*)RVA_S(original);
	}


	_bytes_to_copy = size;
	_callback_function = callback;
	_OVERRIDE = _bytes_to_copy;

	if (original == Offsets::Hooks::RenderGUI) {

		_OVERRIDE = sizeof(fixed_render_gui);
		memcpy(fixed_render_gui + 12, (void*)RVA(Offsets::Screen::Instance), 8);
		memcpy(_original_buffer, fixed_render_gui, _OVERRIDE);
	}
	else memcpy(_original_buffer, _original_league_function, _bytes_to_copy);


}

Whore::Whore(){}

void Whore::Enable() {

	const int buffer_size = sizeof(JUMP_CODE) + _OVERRIDE;
	unsigned char* buffer = (unsigned char*)VirtualAlloc(nullptr, buffer_size, MEM_COMMIT | MEM_RESERVE, PAGE_EXECUTE_READWRITE);
	if (!buffer) return;

	_trampoline = buffer;
	memcpy(buffer, _original_buffer, _OVERRIDE);

	CREATE_JUMP(jump_to_league, _original_league_function + _bytes_to_copy);
	memcpy(buffer + _OVERRIDE, jump_to_league, sizeof(JUMP_CODE));

	CREATE_JUMP(jump_to_callback, _callback_function);
	memcpy(_original_shadow_function, jump_to_callback, _bytes_to_copy);

	for (int i = sizeof(JUMP_CODE); i < _bytes_to_copy; i++)
		*reinterpret_cast<BYTE*>((QWORD)_original_shadow_function + i) = 0x90;
}