#pragma once

#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers
// Windows Header Files
#include <windows.h>
#include <iostream>
#include <string>
#include <fstream>
#include <vector>
#include <array>
#include <map>
#include <unordered_map>
#include <utility>
using namespace std;
#include <ctime>
#include <chrono>
#include <math.h>
//#include <subauth.h>
#include <psapi.h>
#include <type_traits>
#include <algorithm>


#include <d3d11.h>
#include <d3dx11.h>


