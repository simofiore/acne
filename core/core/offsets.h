#pragma once


namespace Offsets {

	//static QWORD const BaseAddress = (QWORD)GetModuleHandle(NULL);
	//static QWORD const StubAddress = (QWORD)GetModuleHandle("stub.dll");
	//static QWORD HiddenModule = 0;
	static QWORD constexpr RiotWindow = 0x522AA98; //13,13 // C6 43 17 01  -> loc mov rcx, riot window

	static QWORD constexpr MaterialRegistryInstance = 0x5279960;  //13,13 // 48 89 01 48 8B 0D ? ? ? ? 48 85 C9 74 64 , mov rcx, material registry
	namespace MaterialRegistry {
		static QWORD constexpr DX11DriverInstance = 0x28;

		namespace DX11Driver {
			static QWORD constexpr SwapChain = 0x1c0;
			static QWORD constexpr Device = 0x2b0; // DX9 only
		}
	}

	static QWORD constexpr RenderPipelineInstance = 0x521EE28; // 13,13 // 0F B6 EA 48 8B 0D ? ? ? ?
	static QWORD constexpr FnRenderUnusualSuspect = 0x50D800; // 13,13 // E8 ? ? ? ? 0F 28 CF 49 8B CC
	namespace RenderPipeline {
		static QWORD constexpr MouseOverData = 0x2e8;
	}

	static QWORD constexpr GameClientInstance = 0x3998640; // FIRST PARAM TO ONGAMEUPDATE
	namespace GameClient {
		static QWORD constexpr State = 0xc;
	}

	static QWORD constexpr SpoofGadget = 0x49C0F;// v 13.13 // jmp     qword ptr [rdi] -> CE pattern scan FF 27

	static QWORD constexpr GameTime = 0x521EE00; // 13.13 // F3 0F 5C 35 ?? ?? ?? ?? 0F 28 F8

	namespace Zoom {
		static QWORD constexpr Instance = 0x521B180; //13.13; // INSIDE GET HEALTH BAR POS -> v14 = *(float *)(sub_7FF794740C50() + 40) / v13; sub == class
		static QWORD constexpr MaxZoom = 0x28;
	}

	//namespace D3DX {
	//	static QWORD constexpr Instance = 0x5243EE8; //13.12hf ; // "Failed to create DX11Query" -> first instruction -> mov eax, static QWORD_30B392C
	//	static QWORD constexpr Hwnd = 0x218;
	//}

	namespace Screen {
		static QWORD constexpr Instance = 0x5274650; //13.13 0x520E088; //   "DISABLE_FOW" dword sotto
		static QWORD constexpr Size = 0xc;
		//static QWORD constexpr FnWorldToScreen = 0xD652C0; // UPDATED 13.8 // 0xD682D0;  // 48 8B D5 E8 ? ? ? ? 48 83 BF ? ? ? ? ?
	}

	static QWORD constexpr ViewMatrix = 0X526C1B0; //13,13 0x5205C70; // // "mProj" -> unk sopra

	namespace NavigationGrid {
		//static QWORD constexpr Instance = 0x213E0E8; //13.12hf
		//static QWORD constexpr FnGetHeightAtPosition = 0XD261E0; //13,11hf 0xD14D90;  // 40 57 0F 28 D9
		static QWORD constexpr FnGetHeightAtPositionA = 0xD6C030; // 13,13
	}

	namespace Manager {
		static QWORD constexpr Objects = 0x2173220; //13.13;  // E8 ? ? ? ? 48 85 C0 74 24 8B 88 ? ? ? ? inside func first mov rcx
		//static QWORD constexpr Heroes = 0x213DC68; //13.12hf 0x210DEF0; //   48 89 45 2F 48 8B 05 ? ? ? ? 45 33 E4
		//static QWORD constexpr Minions = 0x3998608; //13.12hf 0x3968770; // 48 8B 0D ?? ?? ?? ?? E8 ?? ?? ?? ?? EB 07
		//static QWORD constexpr Turrets = 0x51E4D20; //13.12hf 0x51B4D40; // "towercautionRing_Minimal_Fountain_White"	---> 2 above;		 mov     static QWORD_3325A48, esi
		//static QWORD constexpr Missiles = 0x51F6CD0; //13.12hf 0x51C6A50; //// 48 8D 54 24 ? E8 ? ? ? ? 48 8B 7C 24 ? 48 8B 5C 24 ?
	}

	namespace Minimap {
		static QWORD constexpr Instance = 0x521EE60; //13,13 // 74 09 83 FA 01 75 2F  loca -> mov rcx -> minimap
		static QWORD constexpr Hud = 0x320;
		static QWORD constexpr Position = 0x60;
		static QWORD constexpr Size = 0x68;
	}

	namespace HUD {
		static QWORD constexpr Instance = 0x2173230; //13,13; //  0F B6 B1 ? ? ? ? 75 7E mov rcs -> static QWORD constexpr INSIDE GETHPBARPOS
		static QWORD constexpr Camera = 0x18;
		static QWORD constexpr CursorMoveLogic = 0x28;
		static QWORD constexpr CursorTargetLogic = 0x48;
		static QWORD constexpr InputLogic = 0x68;
		//STOP MOVE= 0x86B240
		static QWORD constexpr FnTryRightClick = 0x899380; //13.13  // 44 88 44 24 ? 55 56 41 54
		static QWORD constexpr FnMove = 0x883AE0; //13.13 // E8 ? ? ? ? EB 15 0F B6 55 5F
		static QWORD constexpr FnCastFromSpellInfo = 0x890320; // 13.13 calls cast spell
	}

	namespace Camera {
		static QWORD constexpr ZoomMultiplier = 0x2b0;
		static QWORD constexpr CurrentZoom = 0x2b8;
	}

	static QWORD constexpr IssueOrderCheck = 0x20EE848; // UPDATED 13.12
	static QWORD constexpr FnIssueOrder = 0x2026A0; //13.13 // E8 ?? ?? ?? ?? 8D 43 11

	namespace Chat {
		static QWORD constexpr Instance = 0x39CDF10; //13.13  // 48 8B 54 24 ? 48 8B 0D ? ? ? ? E8 ? ? ? ? 48 8B 0D ? ? ? ? BA ? ? ? ? E8 ? ? ? ? 48 8B 5C 24 ? mov rcs chat client call sub printchat
		static QWORD constexpr IsOpen = 0xC90;
		static QWORD constexpr FnPrintChat = 0x844850; //13.13  // "game_chat_command_help" SOPRA PRIMA SUB // "game_chat_known_commands"  hud_Chat_bounty_ping
		//static QWORD constexpr FnSendChat = 0x689880;  //A1 ? ? ? ? 56 6A FF.
	}

	namespace SpellData {
		static QWORD constexpr Coefficent1 = 0x228;
		static QWORD constexpr Coefficent2 = 0x22c;
		static QWORD constexpr CastTime = 0x2c0;
		static QWORD constexpr CooldownTime = 0x2e4;
		static QWORD constexpr DelayCastOffsetPerce = 0x300;
		static QWORD constexpr DelayTotalTimePerce = 0x304;
		static QWORD constexpr CastRange = 0x45c;
		static QWORD constexpr CastRangeDisplayOverride = 0x478;
		static QWORD constexpr CastRadius = 0x494;
		static QWORD constexpr CastFrame = 0x4ec;
		static QWORD constexpr MissileSpeed = 0x4f0;
		static QWORD constexpr LineWidth = 0x540;
	}

	namespace SpellInfo {
		static QWORD constexpr Name = 0x28;
		static QWORD constexpr SpellDataInstance = 0x60;
	}

	namespace SpellCast {
		static QWORD constexpr SpellInfoInstance = 0x0;
		static QWORD constexpr SlotID = 0x8;
		static QWORD constexpr CasterName = 0x20;
		static QWORD constexpr CasterHandle = 0x88;
		static QWORD constexpr StartPosition = 0xa4;
		static QWORD constexpr EndPosition = 0xb0;
		static QWORD constexpr MousePosition = 0xbc;
		static QWORD constexpr DestinationHandlesArray = 0xe0;
		static QWORD constexpr HasDestinationHandle = 0xe8;
		static QWORD constexpr NumerOfDestinationHandles = 0xec;
		static QWORD constexpr IsSpell = 0x10c;
		static QWORD constexpr IsBasicAttack = 0x112;
		static QWORD constexpr CastedAtTime = 0x12c;

	}

	namespace SpellBook {

		static QWORD constexpr OwnerHandle = 0x30;
		static QWORD constexpr SpellSlotInstance = 0x6D0;

		static QWORD constexpr FnGetSpellState = 0x6E5A90; //13.13  // E8 ? ? ? ? 48 8B CE 44 8B F8
		static QWORD constexpr FnCastSpell = 0x6FCBB0; // 13.13 // "ERROR: Client Tried to cast a spell from an invalid slot: %i.\n"
		//static QWORD constexpr FnHudCastSpell = 0x85A820; //13.12 0x845B60;   // E8 ?? ?? ?? ?? 48 8B 5C 24 ?? C6 87 ?? ?? ?? ?? ?? C6 87 ?? ?? ?? ?? ??
		//static QWORD constexpr ScreenMousePosition = 0x39985D0; //13.12 0x3968738; // 48 8B 0D ?? ?? ?? ?? 4C 8D 45 67

		namespace SpellSlot {

			static QWORD constexpr Level = 0x28;
			static QWORD constexpr CooldownExpire = 0x30;
			static QWORD constexpr Charges = 0x54;
			static QWORD constexpr CoolDown = 0x74;
			static QWORD constexpr TargetingClientInstance = 0x128;
			static QWORD constexpr SpellInfoInstance = 0x130;

			//SpellSlotCharges = 0x5C
			//	SpellSlotTimeCharge = 0x6C

			namespace TargetingClient {
				static QWORD constexpr SourceHandle = 0x10;
				static QWORD constexpr TargetHandle = 0x14;
				static QWORD constexpr StartPosition = 0x18;
				static QWORD constexpr EndPosition = 0x24;
				static QWORD constexpr MousePosition = 0x30;
				static QWORD constexpr MousePosition2 = 0x3c;
			}

		}
	}

	namespace BuffManager {
		static QWORD constexpr OwnerHandle = 0x10;
		static QWORD constexpr BuffVector = 0x10;

		static QWORD constexpr Type = 0x8;
		static QWORD constexpr Script = 0x10;
		static QWORD constexpr StartTime = 0x18;
		static QWORD constexpr ExpireTime = 0x1c;
		static QWORD constexpr Duration = 0x20;
		static QWORD constexpr IsActive = 0x38;
		static QWORD constexpr IsPermanent = 0x88;
		static QWORD constexpr Count = 0x8c;
	}

	namespace Navigation {
		static QWORD constexpr EndPath = 0x14;
		static QWORD constexpr MovementSpeed = 0x2b8;
		static QWORD constexpr IsMoving = 0x2bc;
		static QWORD constexpr PassedWayPoints = 0x2c0;
		static QWORD constexpr CurrentPathStartPosition = 0x2d0;
		static QWORD constexpr CurrentPathEndPosition = 0x2dc;
		static QWORD constexpr NavigationPoints = 0x2e8;
		static QWORD constexpr IsNotDashing = 0x2f9;
		static QWORD constexpr DashSpeed = 0x300;
		static QWORD constexpr MouseClickPosition = 0x348;
		static QWORD constexpr DashStartPosition = 0x354;
		static QWORD constexpr ServerPosition = 0x414;
		static QWORD constexpr Direction = 0x420;

		static QWORD constexpr FnBuildPath = 0xd380b0; // 48 8B C4 48 89 58 10 55 56 57 41 54 41 55 41 56 41 57 48 8D
		static QWORD constexpr FnSmoothPath = 0x8b7870; //40 53 55 41 56 48 83 EC 50 48
	}

	static QWORD constexpr UnderMouseObject = 0x39CDED8; //13.12 0x3968958;  // 48 89 0D ? ? ? ? 48 8D 05 ? ? ? ? 48 89 01 33 D2
	static QWORD constexpr LocalPlayer = 0x522B768; // 13.13;  // 48 8B 05 ? ? ? ? 4C 8B D2 4C 8B C1
	namespace BaseObject {

		static QWORD constexpr Handle = 0x0010; //UPDATED 13.13
		static QWORD constexpr NetworkID = 0xC8; //UPDATED 13.13
		static QWORD constexpr Team = 0x3c; //UPDATED 13.13
		static QWORD constexpr Name = 0x60; //UPDATED 13.13
		static QWORD constexpr Position = 0x280;//0x0f8;  //UPDATED 13.13  //PREVIOUS POS 0x220

		//ENDS 0X288
		//static QWORD constexpr FnIsAlive = 0x22DD60; // 13.12 0x229500;  // E8 ? ? ? ? 84 C0 74 35 48 8D 8F ? ? ? ? 
	}
	namespace AttackableUnit {

		static QWORD constexpr Visibility = 0x310; //UPDATED 13.13
		static QWORD constexpr HealthBarVisibility = 0x311; //UPDATED 13.13

		static QWORD constexpr Invulnerable = 0x4f0; //UPDATED 13.13
		static QWORD constexpr Targetable = 0xeb0; //UPDATED 13.12

		static QWORD constexpr Health = 0x1068;//UPDATED 13.12
		static QWORD constexpr MaxHealth = 0x1080; //UPDATED 13.12
		static QWORD constexpr Mana = 0x0340;//UPDATED 13.12
		static QWORD constexpr MaxMana = 0x0358;//UPDATED 13.12

		static QWORD constexpr fnCanHighlight = 0x2388A0; //13.13 // E8 ?? ?? ?? ?? 84 C0 74 35 48 8D 8F ?? ?? ?? ??
		//ends 0x1198
	}
	namespace Turret {
		static QWORD constexpr TargetNetworkID = 0x40d8;
	}
	namespace Client {
		static QWORD constexpr CombatType = 0x2670; //UPDATED 13.13 -> MELEE 1? 2 == RANGED

		static QWORD constexpr AbilityHaste = 0x14A8; //UPDATED 13.13
		static QWORD constexpr Lethality = 0x15A8;//UPDATED 13.13
		static QWORD constexpr ArmorPenetrationPercentage = 0x1590; //UPDATED 13.13
		static QWORD constexpr MagicPenetrationFlat = 0x158C; //UPDATED 13.13
		static QWORD constexpr MagicPenetrationPercentage = 0x1594; //UPDATED 13.13
		static QWORD constexpr BaseAttackDamage = 0x167C; //UPDATED 13.13
		static QWORD constexpr BonusAttackDamage = 0x15E8; //UPDATED 13.13
		static QWORD constexpr BonusAbilityPower = 0x15F8; //UPDATED 13.13
		static QWORD constexpr PercentAbilityPowerModifier = 0x15FC; //UPDATED 13.13
		static QWORD constexpr TotalArmor = 0x16A4; //UPDATED 13.13
		static QWORD constexpr TotalMagicResist = 0x16AC; //UPDATED 13.13
		static QWORD constexpr AttackRange = 0x1d78;  //UPDATED 13.13


		static QWORD constexpr SizeMultiplier = 0x1694;  //UPDATED 13.13

		static QWORD constexpr CharacterData = 0x35a0; //UPDATED 13.13
		static QWORD constexpr BaseCharacterData = 0x28; //UPDATED 13.13
		static QWORD constexpr HealthBarOffset = 0xc4; //UPDATED 13.13

		static QWORD constexpr BuffInstance = 0x27c0; //UPDATED 13.13
		static QWORD constexpr SpellBookInstance = 0x29F8;  //UPDATED 13.13
		static QWORD constexpr NavigationInstance = 0x3700; // UPDATED 13.13 // E8 ? ? ? ? 44 39 60 08  first [rcx + xxxh]

		static QWORD constexpr Name = 0x3848;  //UPDATED 13.13
		static QWORD constexpr Level = 0x3FB8; //UPDATED 13.13

		static QWORD constexpr FnGetHealthBarPosition = 0x80ABC0; //13,12 0x7F50C0; // ACCESSES MAX ZOOM VALUE from debugger / E8 ? ? ? ? ? ? ? ? F3 0F 10 46 ? ? 4C 8D 4C 24 ? ?
		static QWORD constexpr FnGetBaseDrawPosition = 0x1F5AB0; //13.13 // E8 ? ? ? ? EB 06 49 8B 06

		//ends 0x3ea4
	}
	namespace Hero {

		static QWORD constexpr FnGetAttackDelay = 0x3ABB70; //13.13   // 48 8B CE F3 0F 11 83 ? ? ? ? call sotto == get attack delay
		static QWORD constexpr FnGetAttackCastDelay = 0x3ABA70; //13.13 ; // 48 8B CE F3 0F 11 83 ? ? ? ? call sopra == get attack cast delay
	}
	namespace ObjectTypeHolder {
		static QWORD constexpr Type = 0x8;
	}

	namespace StubModule {
		static QWORD constexpr StubCheck = 0xCEB95F; //STUB THAT CHECKS ITSELF 0xC9D9D5
		//static QWORD constexpr StubCheckTest = 0x5dc26963;
		//static QWORD constexpr StubCheckJmp = 0xceb95f;

		static QWORD constexpr LeagueCheckA = 0xEF7B3a; //FIRST CHECK ON LEAGUE MODULE .TEXT  RBX + RCX = ADDRESS (RCX = BB0)
		//static QWORD constexpr LeagueCheckACmp = 0x11BD3AF1;

		static QWORD constexpr LeagueCheckB = 0xEF7B74;
		//static QWORD constexpr LeagueCheckBMov = 0x13d;

		static QWORD constexpr LeagueCheckC = 0xEF7BA7;

		static QWORD constexpr LeagueCheckD = 0xEF7BDC; //0xEF7BDC
	}

	namespace Hooks {
		static QWORD constexpr GameUpdate = 0x41ABD0;		//13,13 // E8 ? ? ? ? 48 8B 0D ? ? ? ? E8 ? ? ? ? E8 ? ? ? ? 48 8B 90 ? ? ? ? 
		static QWORD constexpr ObjectCreate = 0x399540;	// 13,13  //  E8 ?? ?? ?? ?? 48 8B 3D ?? ?? ?? ?? 8B 9E ?? ?? ?? ??
		static QWORD constexpr ObjectDelete = 0x3AC940;	// 13,13 // 48 8B 0D ? ? ? ? 48 8B D0 48 83 C4 20 5B E9 ? ? ? ? 48 83 C4 20  -> jmp 
		static QWORD constexpr StartSpellCast = 0x7009B0; //13,13 // 40 53 48 83 EC 30 4C 8B 0A
		static QWORD constexpr HandleSpellSound = 0x1E2B00; //13.13
		static QWORD constexpr RenderMouseHovers = 0x50C310; //13,13 // E8 ? ? ? ? 41 80 7C 24 ? ? 0F 84 ? ? ? ? // DRAWS OVER MODELS BUT UNDER HUD AND HEALTHBARS, 
		static QWORD constexpr RenderModels = 0x50CF60; //13,13 // 48 89 5C 24 ? 57 48 81 EC ? ? ? ? 48 8B DA 48 8B F9 48 8B 91 ? ? ? ? // DRAW UNDER MODELS
		static QWORD constexpr RenderGUI = 0x8CE940; // 13.13 // E8 ?? ?? ?? ?? EB 4C E8 ?? ?? ?? ?? 48 8B 0D ?? ?? ?? ??
		static QWORD constexpr TacticalMapDrawPaths = 0x8CFFB0; // 13,13 // 40 53 55 41 56 48 83 EC 50 48 8B E9 
		static QWORD constexpr TestRender = 0xDF82C0;
	}
	//RenderUsualSuspects 0x4e5fa0 // 48 8B C4 48 89 58 18 89
}

#define RVA(address) (Memory::GetMainModule() + address)
#define RVA_H(address) (Memory::GetHiddenModule() + address)
#define RVA_S(address) (Memory::GetStubModule() + address)




//int Offsets::SpellCastSpellInfo = 0x8;
//int Offsets::SpellCastSpellSlot = 0x10;
//int Offsets::SpellCastStart = 0xAC;
//int Offsets::SpellCastEnd = 0xB0;
//int Offsets::SpellCastMousePos = 0xC4;
//int Offsets::SpellCastSrcIdx = 0x90;
//int Offsets::SpellCastNetworkID = 0x9C;
//int Offsets::SpellCastDestIdx = 0xE8;
//int Offsets::SpellCastDirection = 0xDC;
//int Offsets::SpellCastCastTime = 0xF8; //ex: Lux Q 0.250
//int Offsets::SpellCastStartTime = 0x188;
//int Offsets::SpellCastCastTimeEnd = 0x16C; //CastTime + StartTime

//int Offsets::SpellInfoSpellData = 0x60;
//int Offsets::SpellDataMissileName = 0x80;
//
//int Offsets::MissileSpellInfo = 0x2E8;
//int Offsets::MissileSrcIdx = 0x370;
//int Offsets::MissileDestIdx = 0x3C8;
//int Offsets::MissileStartPos = 0x38C;
//int Offsets::MissileEndPos = 0x398;

//int Offsets::ObjBuffManager = 0x2790; //
//int Offsets::BuffManagerEntriesArray = 0x18; //0x10
//int Offsets::BuffEntryBuffType = 0x8; //0x4
//int Offsets::BuffEntryBuff = 0x10; //0x8
//int Offsets::BuffEntryBuffStartTime = 0x18; //0xC
//int Offsets::BuffEntryBuffEndTime = 0x1C; //0x10
//int Offsets::BuffEntryBuffCountAlt = 0x3C //0x24
//int Offsets::BuffEntryBuffCountAlt2 = 0x38; //0x20
//int Offsets::BuffName = 0x8; //0x4