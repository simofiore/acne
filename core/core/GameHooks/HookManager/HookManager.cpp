#include "HookManager.h"
#include "../game_update.h"
#include "../render_models.h"
#include "../tacticalmap_draw_paths.h"
#include "../start_spell_cast.h"
#include "../render_mouse_hovers.h"
#include "../render_gui.h"
#include "../handle_spell_sound.h"

void hook_manager_startup(GameClient::STATE s) {

	if (s == GameClient::PLAYING) {

		HookManager::render_models_hook = Whore(Offsets::Hooks::RenderModels, &RenderModels_Hook, 16); 
		HookManager::render_models_hook.Enable();

		HookManager::render_gui_hook = Whore(Offsets::Hooks::RenderGUI, &RenderGUI_Hook, 17);
		HookManager::render_gui_hook.Enable();

		HookManager::tactical_map_draw_paths = Whore(Offsets::Hooks::TacticalMapDrawPaths, &TacticalMapDrawPaths_Hook, 17);
		HookManager::tactical_map_draw_paths.Enable();

		HookManager::start_spell_cast_hook = Whore(Offsets::Hooks::StartSpellCast, &StartSpellCast_Hook, 15);
		HookManager::start_spell_cast_hook.Enable();

		HookManager::handle_spell_sound_hook = Whore(Offsets::Hooks::HandleSpellSound, &HandleSpellSound_Hook, 15);
		HookManager::handle_spell_sound_hook.Enable();

		HookManager::render_mouse_hovers_hook = Whore(Offsets::Hooks::RenderMouseHovers, &RenderMouseHovers_Hook, 14);
		HookManager::render_mouse_hovers_hook.Enable();

		Engine::PrintChat("Finished hooking", Color(255, 0, 255, 255));
		Event::UnSubscribe(Event::GameUpdate, &hook_manager_startup);
	}

}

void HookManager::Init() {

	Event::Subscribe(Event::GameUpdate, &hook_manager_startup);

	update_hook = Whore(Offsets::Hooks::GameUpdate, &GameUpdate_Hook, 19);
	update_hook.Enable();

}