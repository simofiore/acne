#pragma once
class HookManager
{
public:

	static inline Whore update_hook;

	static inline Whore object_create_hook;
	static inline Whore object_delete_hook;
	static inline Whore start_spell_cast_hook;
	static inline Whore handle_spell_sound_hook;

	static inline Whore render_mouse_hovers_hook;
	static inline Whore render_models_hook;
	static inline Whore render_gui_hook;
	static inline Whore tactical_map_draw_paths;
	static inline Whore test_hook;

	static void Init();
};

