#pragma once

void** RenderMouseHovers_Hook(RenderPipeline* pipe, void* screen_buffer) {

	auto res = reinterpret_cast<void** (*)(RenderPipeline*, void*)>(HookManager::render_mouse_hovers_hook.GetTrampoline())(pipe, screen_buffer);

	Event::Publish(Event::RenderMouseOvers, pipe, screen_buffer);

	return res;

}