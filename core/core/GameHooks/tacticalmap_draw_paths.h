#pragma once

__int64 TacticalMapDrawPaths_Hook(__int64 a1) {

	Renderer::Begin(Renderer::MINIMAP);

	Event::Publish(Event::TacticalMapDrawPaths);

	Renderer::End();

	return reinterpret_cast<__int64(*)(__int64 a1)>(HookManager::tactical_map_draw_paths.GetTrampoline())(a1);
}
