#pragma once

__int64 HandleSpellSound_Hook(__int64 a1, int a2, SpellCast* cast, __int64 a4) {

	auto res = reinterpret_cast<__int64(*)(__int64, int , SpellCast*, __int64 )>(HookManager::handle_spell_sound_hook.GetTrampoline())(a1,a2,cast,a4);

	Event::Publish(Event::HandleSpellSound, a2, cast);

	return res;
}