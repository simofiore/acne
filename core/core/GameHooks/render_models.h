#pragma once

__int64 RenderModels_Hook(__int64 a1, __int64 a2) {


	Renderer::Begin(Renderer::GROUND);

	Event::Publish(Event::RenderModels);

	Renderer::End();

	auto res = reinterpret_cast<__int64(*)(__int64, __int64)>(HookManager::render_models_hook.GetTrampoline())(a1, a2);

	return res;

}