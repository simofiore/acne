#pragma once

__int64 GameUpdate_Hook(GameClient* game_client) {

	if (game_client->GetState() == GameClient::ENDING)
		exit(0);

	ObjectManager::Update();

	static UINT64 last = 0;

	if (GetTickCount64() - last >= 30)
		Event::Publish(Event::GameUpdate, game_client->GetState()), last = GetTickCount64();

	return reinterpret_cast<__int64(*)(GameClient*)>(HookManager::update_hook.GetTrampoline())(game_client);


}