#pragma once

__int64 RenderGUI_Hook(__int64 a1, __int64 a2) {

	auto res = reinterpret_cast<__int64(*)(__int64, __int64)>(HookManager::render_gui_hook.GetTrampoline())(a1, a2);

	Renderer::Begin(Renderer::UNDERHUD);

	Event::Publish(Event::RenderGUI);

	Renderer::End();

	return res;

}