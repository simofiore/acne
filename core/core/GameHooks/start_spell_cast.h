#pragma once

signed __int64 StartSpellCast_Hook(SpellBook* a1, SpellCast* a2) {

	auto res = reinterpret_cast<signed __int64(*)(SpellBook*, void*)>(HookManager::start_spell_cast_hook.GetTrampoline())(a1, a2);

	if (!a1 or !a2) return res;

	Event::Publish(Event::StartSpellCast, a1, a2);

	return res;
}