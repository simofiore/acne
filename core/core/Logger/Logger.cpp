#include "Logger.h"


bool Logger::_init = false;
std::ofstream Logger::_output; 

void Logger::Init() {

	_output.open("mind_control_logs.txt");

	if (!_output.is_open())
		return;

	std::cout.rdbuf(_output.rdbuf());
	_init = true;
}

void Logger::Dispose() {

	_output.close();
	_init = false;

}

//template<typename... Args>
void Logger::Log(bool raw, std::string function_name, std::string content, ...) {

	if (!_init) Init();

	if (raw) {
		auto str = content.c_str();

		char msg[4000];
		va_list va;
		va_start(va, str);
		vsprintf_s(msg, str, va);
		va_end(va);
		_output << msg << " ";
		_output << std::endl;
	}
	else {
		auto now = std::chrono::system_clock::now();
		std::time_t current_time = std::chrono::system_clock::to_time_t(now);
		std::tm* local_time = std::localtime(&current_time);
		auto current_time_point = std::chrono::time_point_cast<std::chrono::milliseconds>(now);
		auto milliseconds = current_time_point.time_since_epoch().count() % 1000;

		std::string s_time = std::to_string(local_time->tm_hour) + ":" + std::to_string(local_time->tm_min) + ":" + std::to_string(local_time->tm_sec) + ":" + std::to_string(milliseconds);

		auto str = content.c_str();

		char msg[4000];
		va_list va;
		va_start(va, str);
		vsprintf_s(msg, str, va);
		va_end(va);

		_output << "[ mind-control ] - ";
		_output << "[" << s_time << "] - ";
		_output << "[" << function_name << "] > ";
		_output << msg << " ";
		_output << std::endl;
	}

}