#pragma once
class Logger
{

	static std::ofstream _output;
	static bool _init;

public:

	static void Init();
	static void Dispose();

	//template<typename... Args>
	static void Log(bool raw, std::string function_name, std::string content, ...);

};

