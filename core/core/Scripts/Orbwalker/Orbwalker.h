#pragma once
class Orbwalker
{
public:

	enum STATE {
		IDLE, // MOVING
		ATTACKING, //  CANT MOVE OR CANCELS AUTO
		CASTING, // NORMAL CAST, CAN MOVE
		CHARGING, // XERATHQ ,CAN MOVE 
		CHANNELING, // KATA R, CANT MOVE
		DODGING // CANT MOVE
	};

	enum MODE {
		NONE,
		COMBO,
		FLEE
	};

private:

	static inline STATE _state = IDLE;
	static inline MODE _mode = NONE;
	static inline float _last_aa = 0;

	static bool CanAttack();
	static bool CanMove();

	static int GetLatency(int extra = 0);

public:

	static void Init();
	static void OnTick();
	static void OnDraw();
	static void OnMouseHovers(RenderPipeline* pipe, void* screen_buffer);
	static void OnCast(SpellBook* book, SpellCast* cast);
	static void OnCastSound(int state, SpellCast* cast);
	static void OnWndProc(UINT msg, WPARAM param);
	static inline void Reset() { _last_aa = 0; }

};

