#include "Orbwalker.h"

static bool is_kalista = false;

bool Orbwalker::CanAttack() {
	return GetTickCount64() >= (_last_aa + (Engine::GetLocalPlayer()->GetAttackDelay() * 1000)); //(Engine::GetGameTime() * 1000)
}
bool Orbwalker::CanMove() {
	if (is_kalista) return true;
	return GetTickCount64() >= (_last_aa + (Engine::GetLocalPlayer()->GetAttackCastDelay() * 1000)); //
}
int Orbwalker::GetLatency(int extra) {
	return  extra + 30 / 2;
}

void Orbwalker::Init() {

	if (Engine::GetLocalPlayer()->GetName() == "Kalista") is_kalista = true;

	Event::Subscribe(Event::WndProc, &OnWndProc);
	Event::Subscribe(Event::RenderModels, &OnDraw);
	Event::Subscribe(Event::RenderMouseOvers, &OnMouseHovers);
	Event::Subscribe(Event::GameUpdate, &OnTick);
	Event::Subscribe(Event::StartSpellCast, &OnCast);
	Event::Subscribe(Event::HandleSpellSound, &OnCastSound);
	Engine::PrintChat("Orbwalker > Loaded.", Color(255, 0, 255, 255));

}

void Orbwalker::OnWndProc(UINT msg, WPARAM param) {


	if (param == VK_SPACE) {
		switch (msg)
		{
		case WM_KEYDOWN: _mode = COMBO; Engine::GetLocalPlayer()->GoldHealthBar(true); break;
		case WM_KEYUP: _mode = NONE; Engine::GetLocalPlayer()->GoldHealthBar(false); break;
		}
	}

	if (param == 86) { //V
		switch (msg)
		{
		case WM_KEYDOWN: _mode = FLEE; break;
		case WM_KEYUP: _mode = NONE; break;
		}
	}

}

void Orbwalker::OnDraw() {
	auto me = Engine::GetLocalPlayer();

	if (!me) return;
	if (!me->IsAlive()) return;

	Draw::Circle3D(me->GetWorldPosition(), me->GetAttackRange(), Color(255,255,255,200));
	
}

void Orbwalker::OnMouseHovers(RenderPipeline* pipe, void* screen_buffer) {
	if (_mode != COMBO) return;

	auto data = *pipe->GetMouseOverData();
	data.EnemyColor = Color(197, 100, 197, 255);
	data.SelfColor = Color(100, 216, 242, 255);

	auto me = Engine::GetLocalPlayer();
	if (me)
		me->Glow(pipe, screen_buffer, &data);

	Hero* target = TargetSelector::GetOverrideTarget();
	if (target)
		target->Glow(pipe, screen_buffer, &data);

}

void Orbwalker::OnTick() {
	if (_mode != COMBO) return;
	auto me = Engine::GetLocalPlayer();
	if (!me) return;
	if (!me->IsAlive()) return;

	if (!CanMove()) return;

	if (_state == CHANNELING or _state == DODGING) return;

	auto book = me->GetSpellBook();
	if (!book) {
		Engine::Move(Engine::GetHUD()->GetMouseWorldPosition());
		return;
	}

	auto current_cast = book->GetActiveSpellCast();
	if (current_cast and current_cast->IsSpell()) {
		Engine::Move(Engine::GetHUD()->GetMouseWorldPosition());
		return;
	}

	auto nav = me->GetNavigation();
	if (nav and nav->IsDashing()) {
		Engine::Move(Engine::GetHUD()->GetMouseWorldPosition());
		return;
	}

	Hero* target = TargetSelector::FindBestTarget(me->GetWorldPosition(), me->GetAttackRange());
	if (!target){
		Engine::Move(Engine::GetHUD()->GetMouseWorldPosition());
		return;
	}

	if (!CanAttack()) {
		Engine::Move(Engine::GetHUD()->GetMouseWorldPosition());
		return;
	}

	Event::Publish(Event::PreBasicAttack);
	Engine::Attack(target);
	_last_aa = GetTickCount64() + GetLatency();//(Engine::GetGameTime() * 1000)
	_state = ATTACKING;
	return;

}

void Orbwalker::OnCast(SpellBook* book, SpellCast* cast) {



	if (cast->GetCasterHandle() != Engine::GetLocalPlayer()->GetHandle()) return;
	if (!cast->IsBasicAttack()) return;

	_last_aa = GetTickCount64() + GetLatency(); //(Engine::GetGameTime() * 1000)
	_state = ATTACKING;
}


void Orbwalker::OnCastSound(int state, SpellCast* cast) {
	if (cast->GetCasterHandle() != Engine::GetLocalPlayer()->GetHandle()) return;
	if (!cast->IsBasicAttack()) return;

	switch (state) {
	case 13: _state = IDLE; Event::Publish(Event::PostBasicAttack /*,cast->GetTargedHandles()[0]*/); break;
	}
}