#pragma once
class TargetSelector
{

	static inline Hero* _last_target;
	static inline  Hero* _override_target;
	static bool IsValid(Hero* target, Vector3 from, float range);

public:

	static void Init();
	static void OnWndProc(UINT msg, WPARAM param);
	static Hero* FindBestTarget(Vector3 from, float range);
	static Hero* GetOverrideTarget() { return _override_target; }
};

