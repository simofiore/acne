#include "TargetSelector.h"
#include "../Utility/DamageCalculator.h"

void TargetSelector::Init() {

	Event::Subscribe(Event::WndProc, &TargetSelector::OnWndProc);
	Engine::PrintChat("TargetSelector > Loaded.", Color(255, 0, 255, 255));
}

void TargetSelector::OnWndProc(UINT msg, WPARAM param) {

	if (msg != WM_LBUTTONDOWN) return;

	auto mouse_entity = Engine::GetUnderMouseObject();
	if (!mouse_entity) return;
	if (mouse_entity->GetType() != Object::AIHeroClient) return;
	if (!mouse_entity->IsEnemyTo(Engine::GetLocalPlayer())) return;

	_override_target = (Hero*)mouse_entity;
}

bool TargetSelector::IsValid(Hero* target, Vector3 from, float range) {

	if (!target) return false;

	if (!target->IsAlive() or !target->IsVisible() or !target->IsTargetable() or target->IsInvulnerable() or target->GetWorldPosition().Distance(from) > range + target->GetBoundingRadius() / 2) return false;

	return true;

}

Hero* TargetSelector::FindBestTarget(Vector3 from, float range) {

	if (IsValid(_override_target, from, range)) return _override_target;

	std::vector<Hero*> possible_targets;
	for (auto hero : ObjectManager::GetHeroes()) {

		if (!hero) continue;

		if (!hero->IsEnemyTo(Engine::GetLocalPlayer())) continue;

		if (hero->IsAlive() and hero->IsVisible() and hero->IsTargetable() and !hero->IsInvulnerable() and hero->GetWorldPosition().Distance(from) <= range + hero->GetBoundingRadius() / 2)
			possible_targets.push_back(hero);

	}

	if (possible_targets.empty()) {
		_last_target = nullptr;
		return nullptr;
	}

	if (possible_targets.size() > 1)
		std::sort(possible_targets.begin(), possible_targets.end(),
			[](Hero* pFirst, Hero* pSecond) -> bool
			{
				auto me = Engine::GetLocalPlayer();
				auto my_aa = me->GetStat(Client::BaseAttackDamage) + me->GetStat(Client::BonusAttackDamage);

				auto damage_first = DamageCalculator::Calculate(me, pFirst, my_aa, DamageCalculator::PHYSICAL);
				auto damage_second = DamageCalculator::Calculate(me, pSecond, my_aa, DamageCalculator::PHYSICAL);

				auto health_first = pFirst->GetStat(Client::Health);
				auto health_second = pSecond->GetStat(Client::Health);

				auto autos_first = health_first / damage_first;
				auto autos_second = health_second / damage_second;

				return autos_first < autos_second;

			});

	_last_target = possible_targets.front();
	return _last_target;
}