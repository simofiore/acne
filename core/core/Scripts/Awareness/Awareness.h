#pragma once
class Awareness
{

	struct Cooldowns {
		float spell[6];
	};
	static inline std::unordered_map<DWORD, Cooldowns*> _cooldown_map;
	static inline std::unordered_map<std::string, void*> _spell_map;
	static inline std::unordered_map<std::string, void*> _hero_map;
	static inline void* _local_texture;

	static void LoadSpellTextures();
	static void LoadHeroTextures();
	static void CreateCooldownMap();

	static void DrawTurretsRange();
	static void DrawHeroesRange();

public:

	static void Init();
	static void OnTick();
	static void OnDraw();
	static void OnDrawUnderHUD();
	static void OnMinimap();
	static void OnMouseHovers(RenderPipeline* pipe, void* screen_buffer);
	static void OnCast(SpellBook* book, SpellCast* cast);
};

