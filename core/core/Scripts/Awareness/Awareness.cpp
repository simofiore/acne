#include "Awareness.h"

std::string ToLowercase(std::string inputString)
{
	std::string lowercaseString = inputString;
	std::transform(lowercaseString.begin(), lowercaseString.end(), lowercaseString.begin(), ::tolower);
	return lowercaseString;
}

void Awareness::CreateCooldownMap() {
	for (auto hero : ObjectManager::GetHeroes()) {
		if (!hero) continue;
		auto cooldown_entry = new Cooldowns();
		_cooldown_map.insert({ hero->GetHandle(), cooldown_entry });
	}
}
void Awareness::LoadSpellTextures() {
	_spell_map.insert({ "SummonerDot", Draw::CreateCompressedTexture((void*)Icons::summoner_ignite, sizeof(Icons::summoner_ignite)) });
	_spell_map.insert({ "SummonerBoost", Draw::CreateCompressedTexture((void*)Icons::summoner_boost, sizeof(Icons::summoner_boost)) });
	_spell_map.insert({ "SummonerExhaust", Draw::CreateCompressedTexture((void*)Icons::summoner_exhaust, sizeof(Icons::summoner_exhaust)) });
	_spell_map.insert({ "SummonerHaste", Draw::CreateCompressedTexture((void*)Icons::summoner_haste, sizeof(Icons::summoner_haste)) });
	_spell_map.insert({ "SummonerHeal", Draw::CreateCompressedTexture((void*)Icons::summoner_heal, sizeof(Icons::summoner_heal)) });
	_spell_map.insert({ "SummonerBarrier", Draw::CreateCompressedTexture((void*)Icons::summoner_barrier, sizeof(Icons::summoner_barrier)) });


	auto flash = Draw::CreateCompressedTexture((void*)Icons::summoner_flash, sizeof(Icons::summoner_flash));
	_spell_map.insert({ "SummonerFlash",  flash });
	_spell_map.insert({ "SummonerFlashPerksHextechFlashtraptionV2",  flash });

	auto smite = Draw::CreateCompressedTexture((void*)Icons::summoner_smite, sizeof(Icons::summoner_smite));
	_spell_map.insert({ "SummonerSmite", smite });
	_spell_map.insert({ "S5_SummonerSmitePlayerGanker", smite });
	_spell_map.insert({ "SummonerSmiteAvatarOffensive", smite });
	_spell_map.insert({ "SummonerSmiteAvatarUtility", smite });
	_spell_map.insert({ "SummonerSmiteAvatarDefensive", smite });


	auto tp = Draw::CreateCompressedTexture((void*)Icons::summoner_teleport, sizeof(Icons::summoner_teleport));
	_spell_map.insert({ "SummonerTeleport", tp });
	_spell_map.insert({ "S12_SummonerTeleportUpgrade", tp });

}
void Awareness::LoadHeroTextures() {

	for (auto hero : ObjectManager::GetHeroes()) {

		auto it = Icons::Map.find(hero->GetName());
		if (it == Icons::Map.end()) { Engine::PrintChat("ERROR: texture for " + hero->GetName() + " not found.", Color(255, 0, 0)); continue; }

		auto file = it->second;
		auto texture = Draw::CreateTexture(file.icon, file.size);
		if (!texture) { Engine::PrintChat("ERROR: texture for " + hero->GetName() + "FAILED.", Color(255, 0, 0)); continue; }

		_hero_map.insert({ hero->GetName(), texture });
	}

}

void Awareness::Init() {

	CreateCooldownMap();
	LoadSpellTextures();
	LoadHeroTextures();
	// LOAD TEXTURES

	Event::Subscribe(Event::RenderModels, &OnDraw);
	Event::Subscribe(Event::RenderGUI, &OnDrawUnderHUD);
	Event::Subscribe(Event::RenderMouseOvers, &OnMouseHovers);
	Event::Subscribe(Event::TacticalMapDrawPaths, &OnMinimap);
	Event::Subscribe(Event::StartSpellCast, &OnCast);

	Engine::PrintChat("Awareness > Loaded.", Color(255, 0, 255, 255));
}


void Awareness::DrawTurretsRange() {
	auto me = Engine::GetLocalPlayer();
	if (!me) return;

	for (auto turret : ObjectManager::GetTurrets())
	{
		if (!turret) continue;
		if (!turret->IsEnemyTo(me)) continue;
		if (!turret->IsVisible()) continue;
		if (!turret->IsAlive()) continue;

		auto col = Color(255, 218, 107, 50);

		if (turret->GetTargetNetworkID()) {
			if (turret->GetTargetNetworkID() == me->GetNetworkID())
				col = Color(255, 0, 0, 100);
			else col = Color(0, 0, 255, 50);
		}

		Draw::Circle3D(turret->GetWorldPosition(), 800 + me->GetBoundingRadius(), col);
	}
}
void Awareness::DrawHeroesRange() {
	auto me = Engine::GetLocalPlayer();
	if (!me) return;

	for (auto hero : ObjectManager::GetHeroes()) {
		if (!hero) continue;
		if (!hero->IsEnemyTo(me)) continue;
		if (!hero->IsAlive()) continue;
		if (!hero->IsVisible()) continue; //HANDLES ON MINIMAP AND ON MOUSE HOVER GLOw!
		if (!hero->IsRanged()) continue;

		Draw::Circle3D(hero->GetWorldPosition(), hero->GetAttackRange() + me->GetBoundingRadius(), Color(255, 0, 0, 150));

		auto it = _hero_map.find(hero->GetName());
		if (it == _hero_map.end()) continue;
		auto texture = it->second;

		Vector3 pos;
		if (!Engine::WorldToScreen(hero->GetWorldPosition(), &pos)) continue;

	}
}
void Awareness::OnDraw() {

	DrawTurretsRange();
	DrawHeroesRange();
}

void Awareness::OnDrawUnderHUD() {

	auto me = Engine::GetLocalPlayer();
	if (!me) return;

	for (auto hero : ObjectManager::GetHeroes()) {
		if (!hero) continue;
		//if (!hero->IsEnemyTo(me)) continue;
		if (!hero->IsAlive()) continue;
		if (!hero->IsVisible()) continue;

		auto it = _cooldown_map.find(hero->GetHandle());
		if (it == _cooldown_map.end()) continue;
		auto cooldowns = it->second;

		Vector3 hp_pos;
		if (!hero->GetHealthBarPosition(&hp_pos)) continue;
		hp_pos.x = (int)hp_pos.x - 46;
		hp_pos.y = (int)hp_pos.y - 2;

		auto spellbook = hero->GetSpellBook();

		// DRAW ABILITIES
		for (int i = 0; i < 4; i++) {
			float offset = 27 * i;

			auto spell = spellbook->GetSpellSlot((SpellBook::SLOT)i);
			if (!spell) continue;

			if (!spell->GetLevel()) {
				Draw::Rectangle2D(Vector3(hp_pos.x + offset, hp_pos.y, 0), Vector3(hp_pos.x + offset + 27, hp_pos.y + 4, 0), true, Color(255, 0, 0));
				Draw::Rectangle2D(Vector3(hp_pos.x + offset, hp_pos.y, 0), Vector3(hp_pos.x + offset + 27, hp_pos.y + 4, 0), false, Color(0, 0, 0));
				continue;
			}

			float cooldown_expire = cooldowns->spell[i];
			if (cooldown_expire < Engine::GetGameTime()) {
				Draw::Rectangle2D(Vector3(hp_pos.x + offset, hp_pos.y, 0), Vector3(hp_pos.x + offset + 27, hp_pos.y + 4, 0), true, Color(0, 255, 0));
				Draw::Rectangle2D(Vector3(hp_pos.x + offset, hp_pos.y, 0), Vector3(hp_pos.x + offset + 27, hp_pos.y + 4, 0), false, Color(0, 0, 0));
				continue;
			}

			float remaining_cd = cooldown_expire - Engine::GetGameTime();
			float cooldown = spell->GetSpellInfo()->GetSpellData()->GetCooldowns()[spell->GetLevel()];
			float percentage = 100.f / cooldown * (cooldown - remaining_cd);
			float perc_bar = 27.f / 100.f * percentage;

			Draw::Rectangle2D(Vector3(hp_pos.x + offset, hp_pos.y, 0), Vector3(hp_pos.x + offset + perc_bar, hp_pos.y + 4, 0), true, Color(255, 255, 0));
			Draw::Rectangle2D(Vector3(hp_pos.x + offset, hp_pos.y, 0), Vector3(hp_pos.x + offset + 27, hp_pos.y + 4, 0), false, Color(0, 0, 0));
		}

		Vector3 summoner_start = Vector3(hp_pos.x + 110, hp_pos.y - 24, 0);
		auto spell_d = spellbook->GetSpellSlot(SpellBook::D);
		if (!spell_d) continue;
		void* texture_d = _spell_map[spell_d->GetSpellInfo()->GetName()];
		if (!texture_d) continue;
		Draw::Texture2D(texture_d, summoner_start, Vector3(summoner_start.x + 23, summoner_start.y + 23, 0));
		float spell_d_cooldown_expire = cooldowns->spell[SpellBook::D];
		if (Engine::GetGameTime() < spell_d_cooldown_expire) {
			Draw::Rectangle2D(summoner_start, Vector3(summoner_start.x + 23, summoner_start.y + 23, 0), true, Color(0, 0, 0, 150));
			float remaining_cd = spell_d_cooldown_expire - Engine::GetGameTime();
			Draw::Text2D(Vector3(summoner_start.x + 11 + 2, summoner_start.y + 11 + 2, 0), std::to_string((int)remaining_cd), true, Color(0, 0, 0, 255));
			Draw::Text2D(Vector3(summoner_start.x + 11, summoner_start.y + 11, 0), std::to_string((int)remaining_cd), true, Color());
		}
		Draw::Rectangle2D(summoner_start, Vector3(summoner_start.x + 23, summoner_start.y + 23, 0), false, Color(0, 0, 0, 255));

		auto spell_f = spellbook->GetSpellSlot(SpellBook::F);
		if (!spell_f) continue;
		void* texture_f = _spell_map[spell_f->GetSpellInfo()->GetName()];
		if (!texture_f) continue;
		Draw::Texture2D(texture_f, Vector3(summoner_start.x + 23, summoner_start.y, 0), Vector3(summoner_start.x + 46, summoner_start.y + 23, 0));
		float spell_f_cooldown_expire = cooldowns->spell[SpellBook::F];
		if (Engine::GetGameTime() < spell_f_cooldown_expire) {
			Draw::Rectangle2D(Vector3(summoner_start.x + 23, summoner_start.y, 0), Vector3(summoner_start.x + 46, summoner_start.y + 23, 0), true, Color(0, 0, 0, 150));
			float remaining_cd = spell_f_cooldown_expire - Engine::GetGameTime();
			Draw::Text2D(Vector3(summoner_start.x + 23 + 11 + 2, summoner_start.y + 11 + 2, 0), std::to_string((int)remaining_cd), true, Color(0,0,0,255));
			Draw::Text2D(Vector3(summoner_start.x + 23 + 11, summoner_start.y + 11, 0), std::to_string((int)remaining_cd), true, Color());
		}
		Draw::Rectangle2D(Vector3(summoner_start.x + 23, summoner_start.y, 0), Vector3(summoner_start.x + 46, summoner_start.y + 23, 0), false, Color(0, 0, 0, 255));

	}
}

void Awareness::OnMouseHovers(RenderPipeline* pipe, void* screen_buffer) {

	auto me = Engine::GetLocalPlayer();
	if (!me) return;

	for (auto hero : ObjectManager::GetHeroes()){
		if (!hero) continue;
		if (!hero->IsEnemyTo(me)) continue;
		if (hero->IsVisible()) continue;

		auto data = *pipe->GetMouseOverData();
		data.EnemyColor = Color(255, 255, 255, 100);

		const auto visibility = reinterpret_cast<bool*>(hero->Hex() + 0x310);
		const auto old_vis = *visibility;
		*visibility = true;
		hero->Glow(pipe, screen_buffer, &data);
		*visibility = old_vis;
	}

}

void Awareness::OnMinimap() {
	auto me = Engine::GetLocalPlayer();
	if (!me) return;

	for (auto hero : ObjectManager::GetHeroes()) {
		if (!hero) continue;
		if (!hero->IsEnemyTo(me)) continue;
		if (!hero->IsAlive()) continue;
		if (hero->IsVisible()) continue; 

		auto it = _hero_map.find(hero->GetName());
		if (it == _hero_map.end()) continue;
		auto texture = it->second;

		auto pos = Engine::WorldToMinimap(hero->GetWorldPosition());

		Draw::Texture2D(texture, Vector3(pos.x - 15, pos.y - 15, 0), Vector3(pos.x + 15, pos.y + 15, 0));
		Draw::Circle2D(pos, 15, Color(255, 255, 255));
	}

}

void Awareness::OnCast(SpellBook* book, SpellCast* cast) {

	if (!cast->IsSpell()) return;
	DWORD caster_handle = cast->GetCasterHandle();

	auto caster = ObjectManager::GetClientByHandle(caster_handle);
	if (!caster) return;
	if (!caster->IsHero()) return;

	auto it = _cooldown_map.find(caster_handle);
	if (it == _cooldown_map.end()) return;

	auto slot = (SpellBook::SLOT)cast->GetSlotID();
	auto spellslot_level = book->GetSpellSlot(slot)->GetLevel();
	float spell_cooldown = cast->GetSpellInfo()->GetSpellData()->GetCooldowns()[spellslot_level];
	float ability_haste = caster->GetStat(Client::AbilityHaste);
	float final_cooldown = Engine::GetGameTime();

	if (slot == SpellBook::D or slot == SpellBook::F) 
	{
		final_cooldown += spell_cooldown;
	}
	else 
	{
		float reduced_cooldown = spell_cooldown * (100 / (100 + ability_haste));
		final_cooldown += reduced_cooldown;
	}

	it->second->spell[slot] = final_cooldown;
}