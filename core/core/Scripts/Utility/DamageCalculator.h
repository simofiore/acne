#pragma once

class DamageCalculator {

public:

	enum TYPE {
		PHYSICAL, MAGICAL, TRUEDAMAGE
	};

	static float Calculate(Client* from, Client* to, float raw, TYPE t);

};

float DamageCalculator::Calculate(Client* from, Client* to, float raw, TYPE t) {

	switch (t) {

	case PHYSICAL: {

		// starting armor
		float target_armor = to->GetStat(Client::TotalArmor);

		// mystats
		float source_pen_perc = from->GetStat(Client::ArmorPenetrationPercentage);
		float source_pen_flat = from->GetStat(Client::Lethality) * (0.6 + 0.4 * from->GetStat(Client::Level) / 18);

		// armor post calc
		target_armor *= source_pen_perc;
		target_armor -= source_pen_flat;

		if (target_armor >= 0) {
			float multiplier = 100.f / (100.f + target_armor);
			return roundf(raw * multiplier);
		}
		else {
			float multiplier = 2 - (100.f / (100.f + target_armor));
			return roundf(raw * multiplier);
		}

		break;

	}

	case MAGICAL: {

		// starting magic resist
		float target_m_resist = to->GetStat(Client::TotalMagicResist);

		// mystats
		float source_pen_perc = from->GetStat(Client::MagicPenetrationPercentage);
		float source_pen_flat = from->GetStat(Client::MagicPenetrationFlat);

		// mr post calc
		target_m_resist *= source_pen_perc;
		target_m_resist -= source_pen_flat;


		if (target_m_resist >= 0) {
			float multiplier = 100.f / (100.f + target_m_resist);
			return roundf(raw * multiplier);
		}
		else {
			float multiplier = 2 - (100.f / (100.f + target_m_resist));
			return roundf(raw * multiplier);
		}

		break;

	}

	case TRUEDAMAGE: return raw;
	}

}