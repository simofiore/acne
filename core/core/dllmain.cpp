#include "Memory/Utility/decrypt_memory_pages.h"
#include "Memory/stub_bypass/stub_bypass.h"

void InitializeScripts() {
    TargetSelector::Init();
    Awareness::Init();
    Orbwalker::Init();

    Event::UnSubscribe(Event::GameUpdate, &InitializeScripts);
}

bool main() {

    if (!Syscaller::Init()) return false;    
    //packman::decrypt_memory_pages_for_dump();

    Memory::Init();                  
    if (!bypass_anticheat()) return false;        

    Renderer::Init();
    HookManager::Init();

    Engine::GetHUD()->GetCamera()->SetZoomMultiplier(2.0);
    Event::Subscribe(Event::GameUpdate, &InitializeScripts);
    return true;
}

BOOL APIENTRY DllMain( HMODULE hModule,
                       DWORD  ul_reason_for_call,
                       LPVOID lpReserved
                     )
{
    if (ul_reason_for_call == DLL_PROCESS_ATTACH)
        return main();

    return false;
}

