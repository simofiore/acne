#include "Draw.h"
#define STB_IMAGE_IMPLEMENTATION  
#include "../../Libraries/stb/image.h"
#include "../../Libraries/stb/decompress.h"

void Draw::Rectangle2D(Vector3 start, Vector3 end, bool filled, Color col) {

	if (filled)    Renderer::GetDrawList().AddRectFilled(start, end, col);
	else		  Renderer::GetDrawList().AddRect(start, end, col);

}

void Draw::Text2D(Vector3 pos, std::string text, bool centered, Color col) {

	if (!centered) {
		Renderer::GetDrawList().AddText(pos, col, text.c_str());
		return;
	}

	ImVec2 tex_size = ImGui::GetFont()->CalcTextSizeA(ImGui::GetFont()->FontSize, FLT_MAX, 0, text.c_str());
	ImVec2 new_pos = ImVec2(pos.x - tex_size.x * 0.5f, pos.y - ImGui::GetFont()->FontSize * 0.5f);

	Renderer::GetDrawList().AddText(new_pos, col, text.c_str());

}

void Draw::Line2D(Vector3 start, Vector3 end, Color col, float thickness) {
	Renderer::GetDrawList().AddLine(start, end, col, thickness);
}

void Draw::Circle2D(Vector3 center, float radius, Color col, float thickness) {
	Renderer::GetDrawList().AddCircle(center, radius, col, 100, thickness);
}

bool Draw::Circle3D(Vector3 pos, float radius, Color col, float thickness, bool recalculate_height) {

	float points = PI * 2.0f / 100;

	for (float flAngle = 0; flAngle < (PI * 2.0f); flAngle += points)
	{
		Vector3 vStart((radius)*cosf(flAngle) + pos.x, (radius)*sinf(flAngle) + pos.z, pos.y);
		Vector3 vEnd((radius)*cosf(flAngle + points) + pos.x, (radius)*sinf(flAngle + points) + pos.z, pos.y);
		Vector3 vStart2 = Vector3(vStart.x, vStart.z, vStart.y);
		Vector3	vEnd2 = Vector3(vEnd.x, vEnd.z, vEnd.y);

		Vector3 vStartScreen;
		if (!Engine::WorldToScreen(vStart2, &vStartScreen, recalculate_height)) return false;

		Vector3 vEndScreen;
		if (!Engine::WorldToScreen(vEnd2, &vEndScreen, recalculate_height)) return false;

		Line2D(vStartScreen, vEndScreen, col, thickness);

	}

	return true;

}

bool Draw::Line3D(Vector3 start, Vector3 end, Color col, float thickness, bool recalculate_height) {
	Vector3 start_2d, end_2d;

	if (!Engine::WorldToScreen(start, &start_2d, recalculate_height)) return false;
	if (!Engine::WorldToScreen(end, &end_2d, recalculate_height)) return false;

	Line2D(start_2d, end_2d, col, thickness);
	true;
}

void* Draw::CreateTexture(void* data, int size) {

	ID3D11ShaderResourceView* pShaderResource = nullptr;

	//Load from byte array into a raw RGBA buffer
	int image_width = 0;
	int image_height = 0;
	unsigned char* image_data = stbi_load_from_memory((unsigned char*)data, size, &image_width, &image_height, NULL, 4);

	// Create texture
	D3D11_TEXTURE2D_DESC desc;
	ZeroMemory(&desc, sizeof(desc));
	desc.Width = image_width;
	desc.Height = image_height;
	desc.MipLevels = 1;
	desc.ArraySize = 1;
	desc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	desc.SampleDesc.Count = 1;
	desc.Usage = D3D11_USAGE_DEFAULT;
	desc.BindFlags = D3D11_BIND_SHADER_RESOURCE;
	desc.CPUAccessFlags = 0;

	ID3D11Texture2D* pTexture = NULL;
	D3D11_SUBRESOURCE_DATA subResource;
	subResource.pSysMem = image_data;
	subResource.SysMemPitch = desc.Width * 4;
	subResource.SysMemSlicePitch = 0;
	Renderer::GetDevice()->CreateTexture2D(&desc, &subResource, &pTexture);

	// Create texture view
	D3D11_SHADER_RESOURCE_VIEW_DESC srvDesc;
	ZeroMemory(&srvDesc, sizeof(srvDesc));
	srvDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	srvDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
	srvDesc.Texture2D.MipLevels = desc.MipLevels;
	srvDesc.Texture2D.MostDetailedMip = 0;
	Renderer::GetDevice()->CreateShaderResourceView(pTexture, &srvDesc, &pShaderResource);
	pTexture->Release();

	stbi_image_free(image_data);

	return (void*)pShaderResource;
	return nullptr;


}

void* Draw::CreateCompressedTexture(void* data, int size) {

	ID3D11ShaderResourceView* pShaderResource = nullptr;

	const unsigned int buf_decompressed_size = stb_decompress_length((unsigned char*)data);
	unsigned char* buf_decompressed_data = (unsigned char*)malloc(buf_decompressed_size);
	stb_decompress(buf_decompressed_data, (unsigned char*)data, size);


	//Load from byte array into a raw RGBA buffer
	int image_width = 0;
	int image_height = 0;
	unsigned char* image_data = stbi_load_from_memory(buf_decompressed_data, buf_decompressed_size, &image_width, &image_height, NULL, 4);

	// Create texture
	D3D11_TEXTURE2D_DESC desc;
	ZeroMemory(&desc, sizeof(desc));
	desc.Width = image_width;
	desc.Height = image_height;
	desc.MipLevels = 1;
	desc.ArraySize = 1;
	desc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	desc.SampleDesc.Count = 1;
	desc.Usage = D3D11_USAGE_DEFAULT;
	desc.BindFlags = D3D11_BIND_SHADER_RESOURCE;
	desc.CPUAccessFlags = 0;

	ID3D11Texture2D* pTexture = NULL;
	D3D11_SUBRESOURCE_DATA subResource;
	subResource.pSysMem = image_data;
	subResource.SysMemPitch = desc.Width * 4;
	subResource.SysMemSlicePitch = 0;
	Renderer::GetDevice()->CreateTexture2D(&desc, &subResource, &pTexture);

	// Create texture view
	D3D11_SHADER_RESOURCE_VIEW_DESC srvDesc;
	ZeroMemory(&srvDesc, sizeof(srvDesc));
	srvDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	srvDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
	srvDesc.Texture2D.MipLevels = desc.MipLevels;
	srvDesc.Texture2D.MostDetailedMip = 0;
	Renderer::GetDevice()->CreateShaderResourceView(pTexture, &srvDesc, &pShaderResource);
	pTexture->Release();

	stbi_image_free(image_data);

	return (void*)pShaderResource;
	return nullptr;

}

void Draw::FreeTexture(void* data) {

	auto p = (ID3D11ShaderResourceView*)data;
	p->Release();

}

void Draw::Texture2D(void* texture, Vector3 start, Vector3 end) {

	Renderer::GetDrawList().AddImage(texture, ImVec2(start.x, start.y), ImVec2(end.x, end.y));
}