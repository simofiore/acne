#pragma once

#include "Libraries/imgui/imgui_internal.h"

class MultiRenderer
{
private:
    std::mutex _muted;
    ImDrawData _draw_data;

    std::vector<ImDrawList> _draw_lists;    // the thread render to these draw lists
    std::vector<ImDrawList> _copy_list;     // calling EndFrame() will copy the _draw_lists to this
    std::vector<ImDrawList*> _copy_pointer; // pointer to each drawlist in _copy_list, needed for _draw_data
    int _current_draw_list;

    ImGuiViewportP* _view_port;
public:

    // numDrawList = number of draw lists needed, 2 for background and foreground
    MultiRenderer(size_t numDrawList = 3, ImGuiViewport* viewport = nullptr)
    {
        _view_port = static_cast<ImGuiViewportP*>(viewport ? viewport : ImGui::GetMainViewport());

        _copy_list.resize(numDrawList, &GImGui->DrawListSharedData);

        for (size_t i = 0; i < numDrawList; i++)
        {
            _draw_lists.emplace_back(&GImGui->DrawListSharedData);
            _copy_pointer.emplace_back(&_copy_list[i]);
        }

        _draw_data.Valid = true;
        _draw_data.CmdLists = _copy_pointer.data();
        _draw_data.CmdListsCount = _copy_pointer.size();
    }

    // call this before begin frame to set the drawlist index
    inline void SetDrawList(int index) {
        _current_draw_list = index;
    }

    // index=0 for the background drawlist, index=1 for the foreground, basically the z-index
    inline ImDrawList& GetDrawList() noexcept
    {
        return _draw_lists[_current_draw_list];
    } 

    inline void BeginFrame()
    {
        for (auto& drawList : _draw_lists)
        {
            drawList._ResetForNewFrame();
            drawList.PushTextureID(GImGui->IO.Fonts->TexID);
            drawList.PushClipRect(_view_port->Pos, ImVec2(_view_port->Pos.x + _view_port->Size.x, _view_port->Pos.y + _view_port->Size.y), false);
        }
    }
    
    inline void EndFrame()
    {
        _muted.lock();

        _copy_list = _draw_lists;

        _draw_data.TotalVtxCount = _draw_data.TotalIdxCount = 0;
        _draw_data.DisplayPos = _view_port->Pos;
        _draw_data.DisplaySize = _view_port->Size;
        _draw_data.FramebufferScale = GImGui->IO.DisplayFramebufferScale;

        for (auto& drawList : _copy_list)
        {
            drawList._PopUnusedDrawCmd();
            _draw_data.TotalVtxCount += drawList.VtxBuffer.Size;
            _draw_data.TotalIdxCount += drawList.IdxBuffer.Size;
        }

        _muted.unlock();
    }

    inline void Render() noexcept
    {
        _muted.lock();
        ImGui_ImplDX11_RenderDrawData(&_draw_data);
        _muted.unlock();
    }
};