#pragma once
class Draw
{

public:

	static void Text2D(Vector3 pos, std::string text, bool centered = true, Color col = Color());
	static void Line2D(Vector3 start, Vector3 end, Color col = Color(), float thickness = 1.f);
	static void Rectangle2D(Vector3 start, Vector3 end, bool filled, Color col = Color());
	static void Circle2D(Vector3 center, float radius, Color col = Color(), float thickness = 1.f);
	static bool Circle3D(Vector3 pos, float radius, Color col = Color(), float thickness = 1.f, bool recalculate_height = false);
	static bool Line3D(Vector3 start, Vector3 end, Color col = Color(), float thickness = 1.f, bool recalculate_height = false);

	static void* CreateTexture(void* data, int size);
	static void FreeTexture(void* data);
	static void* CreateCompressedTexture(void* data, int size);

	static void Texture2D(void* texture, Vector3 start, Vector3 end);

};

