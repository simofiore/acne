#pragma once

class Renderer
{
	static inline IDXGISwapChain* _swapchain = nullptr;
	static inline ID3D11Device* _device = nullptr;
	static inline ID3D11DeviceContext* _context = nullptr;
	static inline ID3D11RenderTargetView* _render_target_view = nullptr;
	static inline HWND _window = NULL;

	static inline BOOL _initialized = false;

public:

	static inline WNDPROC original_wndproc = NULL;

	enum LAYER {
		GROUND,
		UNDERHUD,
		MINIMAP
	};

	static ID3D11Device* GetDevice() { return _device; }
	static ImDrawList& GetDrawList();

	static void Init(int n_list = 3);
	static void Begin(LAYER indx);
	static void End();
};

