#include "Renderer.h"
#include "RendererBackend.h"
static inline MultiRenderer* _thread_renderer = nullptr;




extern LRESULT ImGui_ImplWin32_WndProcHandler(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
QWORD wndproc(const HWND hwnd, const UINT msg, const WPARAM param, const LPARAM lparam) {
	Event::Publish(Event::WndProc, msg, param);
	//if (ImGui_ImplWin32_WndProcHandler(hwnd, msg, param, lparam)) return true;

	return (CallWindowProcA)(Renderer::original_wndproc, hwnd, msg, param, lparam);
}


ImDrawList& Renderer::GetDrawList() {
	return _thread_renderer->GetDrawList();
}

void Renderer::Init(int n_list) {

	if (_initialized) return;

	_window = *(HWND*)(RVA(Offsets::RiotWindow));
	_swapchain = Engine::GetMaterialRegistry()->GetDriver()->GetSwapChain();
	_swapchain->GetDevice(__uuidof(_device), reinterpret_cast<void**>(&(_device)));
	_device->GetImmediateContext(&_context);

	print("window: 0x%p, swapchain: 0x%p, device: 0x%p, context: 0x%p", _window, _swapchain, _device, _context);

	original_wndproc = reinterpret_cast<WNDPROC>((SetWindowLongPtrA)(_window, GWLP_WNDPROC, LONG_PTR(wndproc)));

	ImGui::CreateContext();

	ImGui_ImplWin32_Init(_window);

	// CREATE RENDER TARGET
	ID3D11Texture2D* back_buffer{ nullptr };
	_swapchain->GetBuffer(0u, IID_PPV_ARGS(&back_buffer));

	if (back_buffer) {
		_device->CreateRenderTargetView(back_buffer, NULL, &_render_target_view);
		back_buffer->Release();
	}

	print("render_target_view: 0x%p", _render_target_view);

	ImGui_ImplDX11_Init(_device, _context);

	ImGui::GetIO().FontDefault = ImGui::GetIO().Fonts->AddFontFromFileTTF((R"(C:\Windows\Fonts\arial.ttf)"), 13.f); //14
	ImGui::GetIO().IniFilename = nullptr;
	ImGui::GetIO().LogFilename = nullptr;
	ImGui::GetIO().ConfigFlags |= ImGuiConfigFlags_NoMouseCursorChange;

	ImGui_ImplDX11_CreateDeviceObjects();

	ImGui_ImplDX11_NewFrame();
	ImGui_ImplWin32_NewFrame();
	ImGui::NewFrame();

	_thread_renderer = new  MultiRenderer(n_list);

	ImGui::EndFrame();
	ImGui::Render();

	_context->OMSetRenderTargets(1, &_render_target_view, nullptr);
	ImGui_ImplDX11_RenderDrawData(ImGui::GetDrawData());

	//original_present = (present_fn)vmt_present.Hook((void*)_swapchain, 8, (uintptr_t)&present);

	_initialized = true;
}

void Renderer::Begin(LAYER indx) {
	if (!_initialized) return;
	_thread_renderer->SetDrawList(indx);
	_thread_renderer->BeginFrame();
}

void Renderer::End() {
	if (!_initialized) return;
	_thread_renderer->EndFrame();
	_thread_renderer->Render();

}

