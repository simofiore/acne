#pragma once
class SpellInfo
{
	union {
		DEFINE_MEMBER_N(RiotString _name, Offsets::SpellInfo::Name);
		DEFINE_MEMBER_N(SpellData* _spell_data, Offsets::SpellInfo::SpellDataInstance);
	};

public:

	//DA FARE SPELLDATA
	std::string GetName() { return _name.str(); }
	SpellData* GetSpellData() { return _spell_data; }
};

