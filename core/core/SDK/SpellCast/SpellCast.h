#pragma once
class SpellCast : public Pointer
{
	union {
		DEFINE_MEMBER_0(SpellInfo* _spell_info);
		DEFINE_MEMBER_N(DWORD _slot_id, Offsets::SpellCast::SlotID);
		DEFINE_MEMBER_N(RiotString _caster_name, Offsets::SpellCast::CasterName);
		DEFINE_MEMBER_N(DWORD _caster_handle, Offsets::SpellCast::CasterHandle);
		DEFINE_MEMBER_N(Vector3 _start_position, Offsets::SpellCast::StartPosition);
		DEFINE_MEMBER_N(Vector3 _end_position, Offsets::SpellCast::EndPosition);
		DEFINE_MEMBER_N(Vector3 _mouse_position, Offsets::SpellCast::MousePosition);
		DEFINE_MEMBER_N(bool _is_spell, Offsets::SpellCast::IsSpell);
		DEFINE_MEMBER_N(bool _is_basic_attack, Offsets::SpellCast::IsBasicAttack);
		DEFINE_MEMBER_N(DWORD* _destination_handles, Offsets::SpellCast::DestinationHandlesArray);
		DEFINE_MEMBER_N(bool _has_destination_handles, Offsets::SpellCast::HasDestinationHandle);
		DEFINE_MEMBER_N(DWORD _n_destination_handles, Offsets::SpellCast::NumerOfDestinationHandles);
		DEFINE_MEMBER_N(float _casted_at, Offsets::SpellCast::CastedAtTime);
	};

public:


	SpellInfo* GetSpellInfo() { return _spell_info; }
	DWORD GetSlotID() { return _slot_id; }
	std::string GetCasterName() { return _caster_name.str(); }
	DWORD GetCasterHandle() { return _caster_handle; }
	Vector3 GetStartPosition() { return _start_position; }
	Vector3 GetEndPosition() { return _end_position; }
	Vector3 GetMousePosition() { return _mouse_position; }
	bool IsBasicAttack() { return _is_basic_attack; }
	bool IsSpell() { return !_is_basic_attack and _slot_id >= 0 and _slot_id <= 5; }
	std::vector<DWORD> GetTargedHandles();
	float GetCastTime() { return _casted_at; }

};

