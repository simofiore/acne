#pragma once
class GameClient : public Pointer
{
public:
	enum STATE
	{
		LOADING = 0,
		PLAYING = 2,
		ENDING = 3
	};


private:

	DEFINE_MEMBER_N(STATE _state, Offsets::GameClient::State);

public:

	STATE GetState() { return _state; };
};

