#pragma once
class ObjectManager
{

	static QWORD GetFirst(const QWORD& objectManager);
	static QWORD GetNext(const QWORD& objectManager, const QWORD& obj);

	static inline std::vector<Hero*> _hero_list;
	static inline std::vector<Client*> _minion_list;
	static inline std::vector<Turret*> _turret_list;
	static inline std::vector<AttackableUnit*> _inhibitor_list;
	static inline std::vector<AttackableUnit*> _nexus_list;
	static inline std::vector<Object*> _particle_list;

	static inline std::unordered_map<DWORD, Client*> _client_map;

	static void HandleObject(Object* obj);
	static void Flush();

public:

	static void Update();

	static std::vector<Hero*> GetHeroes() { return _hero_list; }
	static std::vector<Client*> GetMinions() { return _minion_list; }
	static std::vector<Turret*> GetTurrets() { return _turret_list; }
	static std::vector<AttackableUnit*> GetInhibitors() { return _inhibitor_list; }
	static std::vector<AttackableUnit*> GetNexuses() { return _nexus_list; }
	static std::vector<Object*> GetParticles() { return _particle_list; }
	static Client* GetClientByHandle(DWORD handle);
};

