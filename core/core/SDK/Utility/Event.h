#pragma once
class Event
{
public:
	enum ID {
		WndProc,

		GameUpdate,
		ObjectCreate,
		ObjectDelete,
		StartSpellCast,
		HandleSpellSound,
		RenderMouseOvers,
		RenderModels,
		RenderGUI,
		TacticalMapDrawPaths,

		PreBasicAttack,
		PostBasicAttack
	};

	static void Subscribe(ID event, void* callback) {
		_subscribers[event].emplace_back(callback);
	}

	static void UnSubscribe(ID event, void* callback) {

		auto& callbacks = _subscribers[event];
		for (auto it = callbacks.begin(); it != callbacks.end(); it++)
			if (*it == callback) {
				callbacks.erase(it);
				break;
			}
	}

	template <typename... Args>
	constexpr static void Publish(ID event, Args ... args) {
		auto found = _subscribers.find(event);

		if (found == _subscribers.end())
			return;


		for (auto sub : found->second)
			static_cast<void(*)(Args...)>(sub)(args ...);
	}

private:
	static inline std::unordered_map<ID, std::vector<void*>> _subscribers;
};

