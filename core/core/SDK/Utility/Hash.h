#pragma once

// buff names
[[nodiscard]] const static int HashStringCaseInsensitiveFNV1a(const char* a1, int a3 = -2128831035)
{
	int a2 = crt::strlen(a1);
	const char* v3; // esi
	int v4; // edi
	int v5; // ebp
	char v6; // bl
	char v7; // cl
	int v8; // eax

	v3 = a1;
	v4 = a2;
	if (!a2)
		return a3;
	v5 = a3;
	do
	{
		v6 = *v3++;
		v7 = v6 + 32;
		if ((unsigned __int8)(v6 - 65) > 0x19u)
			v7 = v6;
		v8 = v5 ^ v7;
		v5 = 16777619 * v8;
		--v4;
	} while (v4);
	return 16777619 * v8;
}
