#pragma once

class ObjectTypeHolder {
	union
	{
		DEFINE_MEMBER_N(Object::TYPE _type, Offsets::ObjectTypeHolder::Type);
	};

public:

	inline Object::TYPE GetType() { return _type; };
};

