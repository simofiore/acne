#pragma once


#define PI 3.141592653589793238

class Vector3 {
public:
	Vector3() : x(0.f), y(0.f), z(0.f) {}
	Vector3(float _x, float _y, float _z) : x(_x), y(_y), z(_z) {}
	~Vector3() {}

	float x;
	float y;
	float z;

	inline operator ImVec2() const {
		return ImVec2(this->x, this->y);
	}

	inline float Dot(Vector3 v) {
		return x * v.x + y * v.y + z * v.z;
	}

	inline float Distance(Vector3 v) {
		return float(sqrtf(powf(v.x - x, 2.0) + powf(v.y - y, 2.0) + powf(v.z - z, 2.0)));
	}

	Vector3 operator+(Vector3 v) {
		return Vector3(x + v.x, y + v.y, z + v.z);
	}

	Vector3 operator-(Vector3 v) {
		return Vector3(x - v.x, y - v.y, z - v.z);
	}

	Vector3 operator*(float v) {
		return Vector3(x * v, y * v, z * v);
	}

	Vector3 operator/(float v) {
		return Vector3(x / v, y / v, z / v);
	}

	Vector3 operator+=(Vector3 v) {

		x += v.x;
		y += v.y;
		z += v.z;
		return *this;
	}

	Vector3 operator-=(Vector3 v) {

		x -= v.x;
		y -= v.y;
		z -= v.z;
		return *this;
	}

	Vector3 operator*=(Vector3 v) {

		x *= v.x;
		y *= v.y;
		z *= v.z;
		return *this;
	}

	Vector3 operator/=(Vector3 v) {

		x /= v.x;
		y /= v.y;
		z /= v.z;
		return *this;
	}

	bool operator==(Vector3 v) {
		return x == v.x and y == v.y and z == v.z;
	}

	inline float Length() {
		return sqrtf((x * x) + (y * y) + (z * z));
	}

	Vector3 Normalize() {
		float l = Length();
		return Vector3(x / l, y / l, z / l);
	}


	bool Empty() {
		return x == 0 and y == 0 and z == 0;
	}

	bool Equals(Vector3 a) {

		return x == a.x and y == a.y and z == a.z;
	}
};

struct Matrix4x4
{
	static const size_t size = 16;
	float matrix[size];

	float operator [] (size_t i)
	{
		return this->matrix[i];
	}

	void Transpose()
	{
		for (int i = 0; i < 4; i++)
		{
			for (int j = 0; j < i; j++)
			{
				std::swap(matrix[i * 4 + j], matrix[j * 4 + i]);
			}
		}
	}

	// todo, fix it
	//static void MultiplyMatrices(Matrix4x4& out, Matrix4x4& a, Matrix4x4& b)
	//{
	//	MultiplyMatrices(out.matrix, a.matrix, b.matrix);
	//}

	static void MultiplyMatrices(float* out, float* a, float* b)
	{
		for (int i = 0; i < 4; i++)
		{
			for (int j = 0; j < 4; j++)
			{
				float sum = 0.f;
				for (int k = 0; k < 4; k++)
					sum = sum + a[i * 4 + k] * b[k * 4 + j];
				out[i * 4 + j] = sum;
			}
		}
	}
};

struct Vector4
{
	float     x, y, z, w;
	Vector4() { x = y = z = w = 0.0f; }
	Vector4(float _x, float _y, float _z, float _w) { x = _x; y = _y; z = _z; w = _w; }

	float Length()
	{
		return sqrt(x * x + y * y + z * z + w * w);
	}

	float Distance(const Vector4& o)
	{
		return (float)sqrt(pow(x - o.x, 2) + pow(y - o.y, 2) + pow(z - o.z, 2) + pow(w - o.w, 2));
	}

	Vector4 Vscale(const Vector4& s)
	{
		return Vector4(x * s.x, y * s.y, z * s.z, w * s.w);
	}

	Vector4 Scale(float s)
	{
		return Vector4(x * s, y * s, z * s, w * s);
	}

	Vector4 Normalize()
	{
		float l = Length();
		return Vector4(x / l, y / l, z / l, w / l);
	}

	Vector4 Add(const Vector4& o)
	{
		return Vector4(x + o.x, y + o.y, z + o.z, w + o.w);
	}

	Vector4 Sub(const Vector4& o)
	{
		return Vector4(x - o.x, y - o.y, z - o.z, w - o.w);
	}

	Vector4 Clone()
	{
		return Vector4(x, y, z, w);
	}
};


