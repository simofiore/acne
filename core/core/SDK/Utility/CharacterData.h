#pragma once

class BaseCharacterData : public Pointer {
private:
	DEFINE_MEMBER_N(float _healthbar_offset, Offsets::Client::HealthBarOffset);

public:

	float GetHealthBarOffset() { return _healthbar_offset; }
};




class CharacterData : public Pointer {

private:
	union {
		DEFINE_MEMBER_N(BaseCharacterData* _base_character_data, Offsets::Client::BaseCharacterData	);
	};

public:

	BaseCharacterData* GetBaseCharacterData() { return _base_character_data; }

};