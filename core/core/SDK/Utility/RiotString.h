#pragma once


class RiotString
{

	char content[0x10];
	__int64 len = 0;
	__int64 max = 0;

public:

	operator const char* (void)
	{
		return c_str();
	}
	operator std::string(void)
	{
		return std::string(c_str());
	}

	std::string str() {
		return std::string(c_str());
	}
private:
	char* c_str(void)
	{

		if (uintptr_t(this) <= 0x1000)
			return (char*)" ";

		return max >= 31
			? *reinterpret_cast<char**>(content)
			: content;
	}
};