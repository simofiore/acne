#pragma once

#define COL32_R_SHIFT    0
#define COL32_G_SHIFT    8
#define COL32_B_SHIFT    16
#define COL32_A_SHIFT    24


struct Color
{

	float _r, _g, _b, _a;


	Color() { _r = _g = _b = _a = 255; }
	Color(int r, int g, int b, int a = 255) { _r = r; _g = g; _b = b; _a = a; }

	inline operator DWORD() const {
		return  (((UINT)(_a) << COL32_A_SHIFT) | ((UINT)(_b) << COL32_B_SHIFT) | ((UINT)(_g) << COL32_G_SHIFT) | ((UINT)(_r) << COL32_R_SHIFT));
	}

};