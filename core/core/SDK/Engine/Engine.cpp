#include "Engine.h"

MaterialRegistry* Engine::GetMaterialRegistry() {
	return *(MaterialRegistry**)(RVA(Offsets::MaterialRegistryInstance));
}

GameClient* Engine::GetGameClient() {
	return *(GameClient**)(RVA(Offsets::GameClientInstance));
}

float Engine::GetGameTime() {
	return *(float*)(RVA(Offsets::GameTime));
}

void Engine::PrintChat(std::string message, Color col) {
	typedef void(__thiscall* tPrintChat)(QWORD ChatClient, const char* Message, int color);
	tPrintChat fnPrintChat = (tPrintChat)(RVA(Offsets::Chat::FnPrintChat));

	static QWORD chatClient = *(QWORD*)(RVA(Offsets::Chat::Instance));
	if (!chatClient) return;

	char messageBuffer[100];
	sprintf(messageBuffer, "[ M D ] > <font color='#%06X'>%s</font>", (DWORD)col/* & 0x00FFFFFF*/, message.c_str());

	return spoof_call((void*)RVA(Offsets::SpoofGadget), *fnPrintChat, chatClient, (const char*)messageBuffer, 1);


	//return fnPrintChat(chatClient, messageBuffer, 1);
}

Screen* Engine::GetScreen() {

	//auto renderInstance = *(uintptr_t*)(RVA(Offsets::Renderer::Instance));

	//int x = *(int*)(renderInstance + Offsets::Renderer::Width);
	//int y = *(int*)(renderInstance + Offsets::Renderer::Height);
	//return Vector3(x, y, 0);

	return *(Screen**)(RVA(Offsets::Screen::Instance));
}

float Engine::GetHeightAt(Vector3 pos) {
	typedef float(__thiscall* fHeightAt)(float a, float b);
	fHeightAt func = (fHeightAt)(RVA(Offsets::NavigationGrid::FnGetHeightAtPositionA));

	return spoof_call((void*)RVA(Offsets::SpoofGadget), *func, pos.x, pos.z);

	//return func(pos.x, pos.z);
}

Vector3 Engine::WorldToMinimap(Vector3 in) {

	TacticalMap* map = *(TacticalMap**)(RVA(Offsets::Minimap::Instance));

	float dx = in.x / 15000.f;
	float dz = in.z / 15000.f;

	return Vector3((dx * map->GetHUD()->size.x), map->GetHUD()->size.y - (dz * map->GetHUD()->size.y), 0);

}

bool Engine::WorldToScreen(Vector3 pos, Vector3* out, bool recalculate_height) {
	auto viewMatrix = *reinterpret_cast<Matrix4x4*>(RVA(Offsets::ViewMatrix));
	auto projectionMatrix = *reinterpret_cast<Matrix4x4*>(RVA(Offsets::ViewMatrix + 0x40));
	float multipliedMatrix[16];
	Matrix4x4::MultiplyMatrices(multipliedMatrix, viewMatrix.matrix, projectionMatrix.matrix);

	if (recalculate_height)
		pos.y = GetHeightAt(pos);

	Vector4 clipCoords;
	clipCoords.x = pos.x * multipliedMatrix[0] + pos.y * multipliedMatrix[4] + pos.z * multipliedMatrix[8] + multipliedMatrix[12];
	clipCoords.y = pos.x * multipliedMatrix[1] + pos.y * multipliedMatrix[5] + pos.z * multipliedMatrix[9] + multipliedMatrix[13];
	clipCoords.z = pos.x * multipliedMatrix[2] + pos.y * multipliedMatrix[6] + pos.z * multipliedMatrix[10] + multipliedMatrix[14];
	clipCoords.w = pos.x * multipliedMatrix[3] + pos.y * multipliedMatrix[7] + pos.z * multipliedMatrix[11] + multipliedMatrix[15];

	Vector3 returnVec = { 0.f, 0.f, 0.f };
	if (clipCoords.w < 0.1f) return false;

	Vector3 temp;
	temp.x = clipCoords.x / clipCoords.w;
	temp.y = clipCoords.y / clipCoords.w;
	temp.z = clipCoords.z / clipCoords.w;

	returnVec.x = (GetScreen()->GetWidht() / 2.0f * temp.x) + (temp.x + GetScreen()->GetWidht() / 2.0f);
	returnVec.y = -(GetScreen()->GetHeight() / 2.0f * temp.y) + (temp.y + GetScreen()->GetHeight() / 2.0f);

	if (returnVec.x == 0 and returnVec.y == 0) return false;

	*out = returnVec;

	return true;
}

Hero* Engine::GetLocalPlayer() {
	return *(Hero**)(RVA(Offsets::LocalPlayer));
}

Object* Engine::GetUnderMouseObject() {
	auto instance = *(QWORD*)(RVA(Offsets::UnderMouseObject));
	auto retaddr = *(QWORD*)(instance + 0x18);
	return (Object*)retaddr;
}

HUD* Engine::GetHUD() {
	return *(HUD**)(RVA(Offsets::HUD::Instance));
}

Zoom* Engine::GetZoom() {
	return *(Zoom**)(RVA(Offsets::Zoom::Instance));
}

void Engine::Attack(AttackableUnit* target) {
	GetHUD()->RightClick(target->GetWorldPosition());
}

void Engine::Attack(Vector3 pos) {
	GetHUD()->RightClick(pos);
}

void Engine::Move(Vector3 pos, bool fix) {
	if (fix) {
		Vector3 my_pos = Engine::GetLocalPlayer()->GetWorldPosition();
		Vector3 d = pos - my_pos;
		float lenght = d.Length();
		float fraction = 1.f / 5.f;
		Vector3 segment = d * fraction;
		Vector3 final = my_pos + segment;
		GetHUD()->Move(final);
		return;
	}
	GetHUD()->Move(pos);
}