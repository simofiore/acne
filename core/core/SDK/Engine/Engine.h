#pragma once
class Engine
{
public:

	static MaterialRegistry* GetMaterialRegistry();
	static GameClient* GetGameClient();

	static void PrintChat(std::string message, Color col = Color());

	static float GetGameTime();

	static Screen* GetScreen(); //CHANGE TO RENDERER->GETSCREENRESOLUTION
	static float GetHeightAt(Vector3 pos);
	static bool WorldToScreen(Vector3 pos, Vector3* out, bool recalculate_height = false);
	static Vector3 WorldToMinimap(Vector3 pos);

	static Hero* GetLocalPlayer();
	static Object* GetUnderMouseObject();

	enum ALLIANCE {
		ALL,
		ALLIES,
		ENEMIES
	};
	static std::vector<Object*> GetHeroesAs(ALLIANCE type);
	static std::vector<Object*> GetTurretsAs(ALLIANCE type);
	static std::vector<Object*> GetMinionsAs(ALLIANCE type); //DA FARE ANCHE IL CHECK PER I LANE MINION PIANTE ECC
	static std::map<QWORD, Object*> GetMissilesAs(ALLIANCE type);

	static HUD* GetHUD();
	static Zoom* GetZoom();

	static void Attack(AttackableUnit* target);
	static void Attack(Vector3 pos);
	static void Move(Vector3 pos, bool fix = true);
};

