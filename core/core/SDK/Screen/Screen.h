#pragma once

class Screen {
	union {
		DEFINE_MEMBER_N(int _width, Offsets::Screen::Size);
		DEFINE_MEMBER_N(int _height, Offsets::Screen::Size + 0x4);
	};
public:

	int GetWidht() { return _width; }
	int GetHeight() { return _height; }
};