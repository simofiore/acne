#pragma once

class Zoom {
	union {
		DEFINE_MEMBER_N(float _max_zoom, Offsets::Zoom::MaxZoom);
	};
public:
	float GetMaxZoom() { return _max_zoom; }
};
