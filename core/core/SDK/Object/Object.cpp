#include "Object.h"

int Object::GetHandle() {
	return _handle;
}

int Object::GetNetworkID() {
	return _network_id;
}

int Object::GetTeam() {
	return _team;
}

bool Object::IsEnemyTo(Object* target) {
	return this->GetTeam() != target->GetTeam();
}

std::string Object::GetName() {
	//print("%s", _name->str().c_str());

	return _name.str();
}

Vector3 Object::GetWorldPosition() {
	//	print("x: %f, y: %f, z: %f", _position.x, _position.y, _position.z);
	return _position;
}

float Object::GetDistanceTo(Object* target) {
	return this->GetWorldPosition().Distance(target->GetWorldPosition());
}

float Object::GetDistanceTo(Vector3 pos) {
	return this->GetWorldPosition().Distance(pos);
}

Object::TYPE Object::GetType() {
	//typedef QWORD (*OriginalFn)(PVOID);
	//auto holder = CallVirtual<OriginalFn>(this, 1)(this);
	//Object::TYPE _t = *(Object::TYPE*)(holder + 0x8);
	//return _t;


	typedef ObjectTypeHolder* (*OriginalFn)(PVOID);
	auto holder = CallVirtual<OriginalFn>(this, 1)(this);
	return holder->GetType();

	auto pointg = CallVirtual<ObjectTypeHolder*>(this, 1);
	return pointg->GetType();
}

bool Object::IsHero() {
	return this->GetType() == TYPE::AIHeroClient;
}

bool Object::IsMinion() {
	return this->GetType() == TYPE::AIMinionClient;
}

bool Object::IsTurret() {
	return this->GetType() == TYPE::AITurretClient;
}

bool Object::IsMissile() {
	return this->GetType() == TYPE::MissileClient;
}




