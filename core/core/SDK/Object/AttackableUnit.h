#pragma once
class AttackableUnit : public Object
{
protected:

#define DEFINE_PADDING sizeof(Object)

	union {
		DEFINE_MEMBER_N(float _health, Offsets::AttackableUnit::Health);
		DEFINE_MEMBER_N(float _max_health, Offsets::AttackableUnit::MaxHealth);
		DEFINE_MEMBER_N(float _mana, Offsets::AttackableUnit::Mana);
		DEFINE_MEMBER_N(float _max_mana, Offsets::AttackableUnit::MaxMana);
		DEFINE_MEMBER_N(bool _visibility, Offsets::AttackableUnit::Visibility); // DA CAMBIARE TIPO PER LE FLAGS!
		DEFINE_MEMBER_N(bool _healthbar_visibility, Offsets::AttackableUnit::HealthBarVisibility); // DA CAMBIARE TIPO PER LE FLAGS!
		DEFINE_MEMBER_N(DWORD _invulnerable, Offsets::AttackableUnit::Invulnerable); // NON FUNZIONA
		DEFINE_MEMBER_N(bool _targetable, Offsets::AttackableUnit::Targetable);
	};

#define DEFINE_PADDING 0

public:

	bool IsAlive();
	float GetHealthP();
	float GetManaP();
	bool IsVisible();
	bool IsInvulnerable();
	bool IsTargetable();

	void GoldHealthBar(bool on) { _invulnerable = on; }

	void Glow(RenderPipeline* pipe, void* screen_buffer, MouseOverData* data);
};

