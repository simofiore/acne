#pragma once
class Client : public AttackableUnit
{
protected:
#define DEFINE_PADDING sizeof(AttackableUnit)
	union {
		DEFINE_MEMBER_N(float _attack_range, Offsets::Client::AttackRange);
		DEFINE_MEMBER_N(DWORD _combat_type, Offsets::Client::CombatType);
		DEFINE_MEMBER_N(float _ability_haste, Offsets::Client::AbilityHaste);
		DEFINE_MEMBER_N(float _lethality, Offsets::Client::Lethality);
		DEFINE_MEMBER_N(float _armor_penetration_percentage, Offsets::Client::ArmorPenetrationPercentage);
		DEFINE_MEMBER_N(float _magic_penetration_flat, Offsets::Client::MagicPenetrationFlat);
		DEFINE_MEMBER_N(float _magic_penetration_percentage, Offsets::Client::MagicPenetrationPercentage);
		DEFINE_MEMBER_N(float _basic_attack_damage, Offsets::Client::BaseAttackDamage);
		DEFINE_MEMBER_N(float _bonus_attack_damage, Offsets::Client::BonusAttackDamage);
		DEFINE_MEMBER_N(float _bonus_ability_power, Offsets::Client::BonusAbilityPower);
		DEFINE_MEMBER_N(float _percent_ability_power_modifier, Offsets::Client::PercentAbilityPowerModifier);
		DEFINE_MEMBER_N(float _total_armor, Offsets::Client::TotalArmor);
		DEFINE_MEMBER_N(float _total_magic_resist, Offsets::Client::TotalMagicResist);
		DEFINE_MEMBER_N(float _size_multiplier, Offsets::Client::SizeMultiplier);
		DEFINE_MEMBER_N(CharacterData* _character_data, Offsets::Client::CharacterData);
		DEFINE_MEMBER_N(BuffManager _buff_manager, Offsets::Client::BuffInstance);
		DEFINE_MEMBER_N(SpellBook _spell_book, Offsets::Client::SpellBookInstance);
		DEFINE_MEMBER_N(QWORD _encrypted_navigation, Offsets::Client::NavigationInstance);
		DEFINE_MEMBER_N(RiotString _name, Offsets::Client::Name);
		DEFINE_MEMBER_N(DWORD _level, Offsets::Client::Level);

	};
#define DEFINE_PADDING 0
public:
	enum STAT {
		Health,
		MaxHealth,
		HealthPercentage,
		Mana,
		MaxMana,
		ManaPercentage,
		Lethality,
		ArmorPenetrationPercentage,
		MagicPenetrationFlat,
		MagicPenetrationPercentage,
		BaseAttackDamage,
		BonusAttackDamage,
		//PercentAttackDamageModifier,
		BonusAbilityPower,
		PercentAbilityPowerModifier,
		TotalArmor,
		TotalMagicResist,
		Level,
		AbilityHaste
	};

	std::string GetName();
	float GetBoundingRadius();
	float GetAttackRange();
	float GetStat(STAT s);
	bool IsRanged();
	//float GetSizeMultiplier();
	//float GetHealthBarOffset();
	bool GetHealthBarPosition(Vector3* out);
	BuffManager* GetBuffs();
	SpellBook* GetSpellBook();
	Navigation* GetNavigation();

};

