#include "Client.h"

float Client::GetBoundingRadius() {

	typedef float(* OriginalFn)(PVOID);
	return CallVirtual<OriginalFn>(this, 36)(this);
}

float Client::GetAttackRange() {
	return _attack_range + GetBoundingRadius();
}

float Client::GetStat(Client::STAT s) {
	switch (s) {
	case Health: return _health;
	case MaxHealth: return _max_health;
	case HealthPercentage: return GetHealthP();
	case Mana: return _mana;
	case MaxMana: return _max_mana;
	case ManaPercentage: return GetManaP();
	case Lethality: return _lethality;
	case ArmorPenetrationPercentage: _armor_penetration_percentage;
	case MagicPenetrationFlat: return _magic_penetration_flat;
	case MagicPenetrationPercentage: return _magic_penetration_percentage;
	case BaseAttackDamage: return _basic_attack_damage;
	case BonusAttackDamage: return _bonus_attack_damage;
	case BonusAbilityPower: return _bonus_ability_power;
	case PercentAbilityPowerModifier: return _percent_ability_power_modifier;
	case TotalArmor: return _total_armor;
	case TotalMagicResist: return _total_magic_resist;
	case Level: return (float)_level;
	case AbilityHaste: return _ability_haste;
	default: return 0;

	}
}

bool Client::IsRanged() {
	return _combat_type == 2;
}

bool Client::GetHealthBarPosition(Vector3* out) {

	typedef _int64(*fGetBaseDraw)(Client* a1, Vector3* a2);
	fGetBaseDraw fun = (fGetBaseDraw)(RVA(Offsets::Client::FnGetBaseDrawPosition));

	Vector3 pos;
	spoof_call((void*)RVA(Offsets::SpoofGadget), *fun, this, &pos);
	//fun(this, &pos);

	auto a1 = _character_data->GetBaseCharacterData();
	auto bar_offset = a1->GetHealthBarOffset();

	float current_zoom = Engine::GetHUD()->GetCamera()->GetCurrentZoom();
	float max_zoom = Engine::GetZoom()->GetMaxZoom();
	float delta = max_zoom / current_zoom;

	if (!Engine::WorldToScreen(pos, out)) return false;

	out->y -= (Engine::GetScreen()->GetHeight() * 0.00083333335f * delta) * bar_offset;

	return true;

}

SpellBook* Client::GetSpellBook() {
	return &_spell_book;
}

BuffManager* Client::GetBuffs() {
	return &_buff_manager;
}

Navigation* Client::GetNavigation() {

	unsigned __int8* v1; // rdx
	unsigned __int64 v2; // rcx
	__int64 v3; // rax
	unsigned __int64 v4; // r8
	__int64 v5; // r9
	unsigned __int64 v6; // rax
	__int64 v8; // [rsp+8h] [rbp+8h]

	v1 = (unsigned __int8*)(&_encrypted_navigation);
	v2 = 0i64;
	v3 = v1[16];
	v4 = v1[1];
	v5 = *(QWORD*)&v1[8 * v3 + 24];
	v8 = *(QWORD*)&v1[8 * v3 + 24];
	if (v4)
	{
		do
		{
			*(&v8 + v2) ^= ~*(QWORD*)&v1[8 * v2 + 8];
			++v2;
		} while (v2 < v4);
		v5 = v8;
	}
	if (!v1[2])
		return (Navigation*)*(QWORD*)(v5 + 16);
	v6 = 8i64 - v1[2];
	if (v6 >= 8)
		return (Navigation*)*(QWORD*)(v5 + 16);
	do
	{
		*((BYTE*)&v8 + v6) ^= ~v1[v6 + 8];
		++v6;
	} while (v6 < 8);
	return (Navigation*)*(QWORD*)(v8 + 16);

}

std::string Client::GetName() {
	return _name.str();
}
