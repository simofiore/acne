#include "AttackableUnit.h"

float AttackableUnit::GetHealthP() {
	//print("health: %f, max_health: %f", _health, _max_health);
	return _health / _max_health * 100;
}

float AttackableUnit::GetManaP() {
	//print("health: %f, max_health: %f", _health, _max_health);
	return _mana / _max_mana * 100;
}

bool AttackableUnit::IsVisible() {
	return _visibility == 1;
}

bool AttackableUnit::IsTargetable() {
	return _targetable == 1;
}

bool AttackableUnit::IsInvulnerable() {
	return _invulnerable & 1;
}

bool AttackableUnit::IsAlive() {
	typedef bool(* _fnIsAslive)(AttackableUnit* pObj);
	static _fnIsAslive funCanll = (_fnIsAslive)(RVA(Offsets::AttackableUnit::fnCanHighlight));

	auto res = funCanll(this);
	//print("return value: %d", res);
	return res;
}

void AttackableUnit::Glow(RenderPipeline* pipe, void* screen_buffer,  MouseOverData* data) {

	reinterpret_cast<QWORD(*)(void*, DWORD, AttackableUnit*, AttackableUnit*, AttackableUnit*, AttackableUnit*, AttackableUnit*, float, MouseOverData*)>(RVA(Offsets::FnRenderUnusualSuspect))(screen_buffer, 0, Engine::GetLocalPlayer(), this, this, 0, 0, 5.0f, data);

}