#pragma once
class MaterialRegistry
{

	union {
		DEFINE_MEMBER_N(DXDriver* _driver, Offsets::MaterialRegistry::DX11DriverInstance);
	};

public:

	DXDriver* GetDriver() { return _driver; }
};

