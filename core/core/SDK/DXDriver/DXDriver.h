#pragma once
class DXDriver
{
	union {
		DEFINE_MEMBER_N(IDXGISwapChain* _swapchain, Offsets::MaterialRegistry::DX11Driver::SwapChain);
		DEFINE_MEMBER_N(ID3D11Device* _device, Offsets::MaterialRegistry::DX11Driver::Device);
	};

public:

	IDXGISwapChain* GetSwapChain() {return _swapchain;}
	ID3D11Device* GetDevice() { return _device; }

};

