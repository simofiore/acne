#include "SpellBook.h"

SpellSlot* SpellBook::GetSpellSlot(SpellBook::SLOT s) { 

	return *(SpellSlot**)((QWORD)this + Offsets::SpellBook::SpellSlotInstance + (0x8 * (int)s));
}

SpellBook::STATE SpellBook::GetSpellSlotState(SpellBook::SLOT s) { 
	

	typedef STATE(*fnNewIssueOrder)(SpellBook*, SLOT, uintptr_t);
	fnNewIssueOrder fun = (fnNewIssueOrder)(RVA(Offsets::SpellBook::FnGetSpellState)); // 0x8396B0

	//return spoof_call((void*)RVA(Offsets::SpoofGadget), *fun, this, s, (uintptr_t)NULL);

	return reinterpret_cast<STATE(*)(void*, const SLOT, const uintptr_t&)> (RVA(Offsets::SpellBook::FnGetSpellState)) (this, s, NULL);
}

SpellCast* SpellBook::GetActiveSpellCast() {

	auto p = *(uintptr_t*)((uintptr_t)this + 0x38);
	if (!p) return nullptr;
	return (SpellCast*)(p + 0x8);

}