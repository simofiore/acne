#pragma once
class SpellSlot
{
	union {
		DEFINE_MEMBER_N(DWORD _level, Offsets::SpellBook::SpellSlot::Level);
		DEFINE_MEMBER_N(float _cooldown_expire, Offsets::SpellBook::SpellSlot::CooldownExpire);
		DEFINE_MEMBER_N(DWORD _charges, Offsets::SpellBook::SpellSlot::Charges);
		DEFINE_MEMBER_N(float _cooldown, Offsets::SpellBook::SpellSlot::CoolDown);
		DEFINE_MEMBER_N(SpellInfo* _spell_info, Offsets::SpellBook::SpellSlot::SpellInfoInstance);
		DEFINE_MEMBER_N(TargetingClient* _targeting_client, Offsets::SpellBook::SpellSlot::TargetingClientInstance);
	};

public:

	int GetLevel() { return _level; }
	float GetCooldownExpire() { return _cooldown_expire; }
	float GetCooldown() { return _cooldown; }
	int GetCharges() { return _charges; }

	SpellInfo* GetSpellInfo() { return _spell_info; }
	TargetingClient* GetTargetingClient() { return _targeting_client; }


	bool IsReady();
	float ReadyIn();
	float ReadyP();

};

