#pragma once
class RenderPipeline : public Pointer
{

	union {
		DEFINE_MEMBER_N(MouseOverData* _data, Offsets::RenderPipeline::MouseOverData);
	};

public:

	MouseOverData* GetMouseOverData() {return _data;}
};

