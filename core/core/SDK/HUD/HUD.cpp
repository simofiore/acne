#include "HUD.h"

Vector3 HUD::GetMouseWorldPosition() {
	return *(Vector3*)(_cursor_move_logic + 0x2c);
}

void HUD::RightClick(Vector3 world_pos, bool champion_only) {
	Vector3 screenPos;
	if (!Engine::WorldToScreen(world_pos, &screenPos)) return;

	typedef char(*fnNewIssueOrder)(QWORD target_logic, int  a1, int a2, int a3, int x, int y, int a4);
	fnNewIssueOrder fun = (fnNewIssueOrder)(RVA(Offsets::HUD::FnTryRightClick)); // 0x8396B0


	if(champion_only)
		*(bool*)(*(INT64*)((QWORD)this + 0x60) + 0x30) = 1;

	spoof_call((void*)RVA(Offsets::SpoofGadget), *fun, _cursor_target_logic, 0, 0, 1, (int)screenPos.x, (int)screenPos.y, 0);

	//fun(_cursor_target_logic, 0, 0, true, (int)screenPos.x, (int)screenPos.y, 0);

	if (champion_only)
		*(bool*)(*(INT64*)((QWORD)this + 0x60) + 0x30) = 0;

}

void HUD::Move(Vector3 world_pos) {
	Vector3 screenPos;
	if (!Engine::WorldToScreen(world_pos, &screenPos)) return;

	typedef char(__fastcall* fnHudMove)(QWORD cursor_logic, int a2, int a3, int a4, int a5, int a6);
	fnHudMove fun = (fnHudMove)(RVA(Offsets::HUD::FnMove));




	spoof_call((void*)RVA(Offsets::SpoofGadget), *fun, _cursor_move_logic, (int)screenPos.x, (int)screenPos.y, 0, 0, 0);

	//fun(_cursor_move_logic, (int)screenPos.x, (int)screenPos.y, 0, 0, 0);
}

void HUD::CastFromInfo(SpellBook::SLOT slot, Vector3 pos) {

	typedef char(__fastcall* fnHudSpellCast)(QWORD input_logic, SpellInfo* spell);
	fnHudSpellCast fun = (fnHudSpellCast)(RVA(Offsets::HUD::FnCastFromSpellInfo));

	auto slot_instance = Engine::GetLocalPlayer()->GetSpellBook()->GetSpellSlot(slot);
	auto targeting_client = slot_instance->GetTargetingClient();
	targeting_client->SetSource(Engine::GetLocalPlayer()->GetHandle());
	targeting_client->SetStartPosition(Engine::GetLocalPlayer()->GetWorldPosition());
	targeting_client->SetEndPosition(GetMouseWorldPosition());
	targeting_client->SetMousePosition(GetMouseWorldPosition()); 
	targeting_client->SetMousePosition2(GetMouseWorldPosition());

	spoof_call((void*)RVA(Offsets::SpoofGadget), *fun, _input_logic, slot_instance->GetSpellInfo());

	//fun(_input_logic, slot_instance->GetSpellInfo());
}