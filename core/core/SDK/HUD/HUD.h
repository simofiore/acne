#pragma once
class HUD
{
	union {
		DEFINE_MEMBER_N(Camera* _camera, Offsets::HUD::Camera);
		DEFINE_MEMBER_N(QWORD _cursor_move_logic, Offsets::HUD::CursorMoveLogic);
		DEFINE_MEMBER_N(QWORD _cursor_target_logic, Offsets::HUD::CursorTargetLogic);
		DEFINE_MEMBER_N(QWORD _input_logic, Offsets::HUD::InputLogic);
	};

public:

	Camera* GetCamera() { return _camera; }
	Vector3 GetMouseWorldPosition();
	//HUD ISSUE ORDER 
	//HUD CAST SPELL
	void RightClick(Vector3 world_pos, bool champion_only = true);
	void Move(Vector3 world_pos);
	void CastFromInfo(SpellBook::SLOT slot, Vector3 pos);
};

