#pragma once
class Camera
{
	union {
		DEFINE_MEMBER_N(float _zoom_multiplier, Offsets::Camera::ZoomMultiplier);
		DEFINE_MEMBER_N(float _current_zoom, Offsets::Camera::CurrentZoom);
	};

public:

	void SetZoomMultiplier(float new_value) { _zoom_multiplier = new_value; }
	inline float GetCurrentZoom() { return _current_zoom; }
};

