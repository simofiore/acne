#pragma once
class Navigation : public Pointer
{
public:
	union {
		DEFINE_MEMBER_N(Vector3 _end_path, Offsets::Navigation::EndPath);
		DEFINE_MEMBER_N(float _movement_speed, Offsets::Navigation::MovementSpeed);
		DEFINE_MEMBER_N(bool _is_moving, Offsets::Navigation::IsMoving);
		DEFINE_MEMBER_N(INT8 _passed_waypoints, Offsets::Navigation::PassedWayPoints);
		DEFINE_MEMBER_N(Vector3 _current_path_start_position, Offsets::Navigation::CurrentPathStartPosition);
		DEFINE_MEMBER_N(Vector3 _current_path_end_position, Offsets::Navigation::CurrentPathEndPosition);
		DEFINE_MEMBER_N(Vector3* _current_path, Offsets::Navigation::NavigationPoints);
		DEFINE_MEMBER_N(bool _is_not_dashing, Offsets::Navigation::IsNotDashing);
		DEFINE_MEMBER_N(float _dash_speed, Offsets::Navigation::DashSpeed);
		DEFINE_MEMBER_N(Vector3 _mouse_click_position, Offsets::Navigation::MouseClickPosition);
		DEFINE_MEMBER_N(Vector3 _dash_start_position, Offsets::Navigation::DashStartPosition);
		DEFINE_MEMBER_N(Vector3 _server_position, Offsets::Navigation::ServerPosition);
		DEFINE_MEMBER_N(Vector3 _direction, Offsets::Navigation::Direction);
	};



	Vector3 GetEndPath() { return _server_position; }
	Vector3 GetServerPosition() { return _end_path; }
	Vector3 GetDirection() { return _direction; }
	bool IsMoving() { return _is_moving; }
	bool IsDashing() { return !_is_not_dashing; }
	float GetMovementSpeed() { return _movement_speed; }
	float GetDashSpeed() { return _dash_speed; }

	std::vector<Vector3> GetPathing();
};

