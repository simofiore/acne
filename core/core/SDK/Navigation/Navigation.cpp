#include "Navigation.h"

std::vector<Vector3> Navigation::GetPathing() {

	std::vector<Vector3> points;
	Vector3 current;

	int i = 0;

	while (current != _current_path_end_position) {
		current = _current_path[i];
		points.emplace_back(current);
		i++;
	}

	points.emplace_back(_current_path_end_position);
	return points;

}
