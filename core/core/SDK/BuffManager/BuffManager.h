#pragma once
#include <utility>

class BuffManager
{
	union {
		DEFINE_MEMBER_N(DWORD _owner_handle, Offsets::BuffManager::OwnerHandle);
	};

public:

	std::vector<Buff*> GetActives();
};

