#include "BuffManager.h"

std::vector<Buff*> BuffManager::GetActives() {

	std::vector<Buff*> result;

	QWORD begin = *(QWORD*)((QWORD)this + 0x18);
	QWORD end = *(QWORD*)((QWORD)this + 0x20);

	if (!begin or !end) return result;
	for (uintptr_t entry = begin; entry != end; entry += 0x10) {

		Buff* inst = *(Buff**)(entry);
		if (!inst) continue;
		if (!inst->GetScript()) continue;

		if (inst->IsAlive())
			result.push_back(inst);
	}
	return result;

}