#pragma once
class Script
{
	union 
	{
		DEFINE_MEMBER_N(char _name[100], 0x8);
	};

public:

	std::string GetName() { return std::string(_name); }
	int GetNameHash() { return HashStringCaseInsensitiveFNV1a(GetName().c_str()); }
};

