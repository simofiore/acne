#include "Buff.h"

bool Buff::IsAlive() {
	return IsActive	() and (Engine::GetGameTime() > _start_time) and (Engine::GetGameTime() < _expire_time);
}
