#pragma once


class Buff
{
public:
	enum class TYPE {
		Internal = 0,
		Aura = 1,
		CombatEnhancer = 2,
		CombatDehancer = 3,
		SpellShield = 4,
		Stun = 5,
		Invisibility = 6,
		Silence = 7,
		Taunt = 8,
		Berserk = 9,
		Polymorph = 10,
		Slow = 11,
		Snare = 12,
		Damage = 13,
		Heal = 14,
		Haste = 15,
		SpellImmunity = 16,
		PhysicalImmunity = 17,
		Invulnerability = 18,
		AttackSpeedSlow = 19,
		NearSight = 20,
		Fear = 22,
		Charm = 23,
		Poison = 24,
		Suppression = 25,
		Blind = 26,
		Counter = 27,
		Currency = 21,
		Shred = 28,
		Flee = 29,
		Knockup = 30,
		Knockback = 31,
		Disarm = 32,
		Grounded = 33,
		Drowsy = 34,
		Asleep = 35,
		Obscured = 36,
		ClickproofToEnemies = 37,
		UnKillable = 38
	};
private:

	union {
		DEFINE_MEMBER_N(TYPE _type, Offsets::BuffManager::Type);
		DEFINE_MEMBER_N(Script* _script, Offsets::BuffManager::Script);
		DEFINE_MEMBER_N(float _start_time, Offsets::BuffManager::StartTime);
		DEFINE_MEMBER_N(float _expire_time, Offsets::BuffManager::ExpireTime);
		DEFINE_MEMBER_N(float _duration, Offsets::BuffManager::Duration);
		DEFINE_MEMBER_N(bool _is_active, Offsets::BuffManager::IsActive);
		DEFINE_MEMBER_N(bool _is_permanent, Offsets::BuffManager::IsPermanent);
		DEFINE_MEMBER_N(int _count, Offsets::BuffManager::Count);
	};

public:

	bool Is(TYPE t) {return _type == t;}
	int GetCount() { return _count; }
	float GetDuration() { return _duration; }
	float GetExpireTime() { return _expire_time; }
	bool IsActive() { return _is_active; }
	Script* GetScript() { return _script; }

	bool IsAlive();
};

