#pragma once
class HUDMap : public Pointer
{
public:

	union {
		DEFINE_MEMBER_N(Vector3 position, Offsets::Minimap::Position);
		DEFINE_MEMBER_N(Vector3 size, Offsets::Minimap::Size);
	};


};

