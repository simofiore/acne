#pragma once
class TacticalMap : public Pointer
{
	union {
		DEFINE_MEMBER_N(HUDMap* _hud, Offsets::Minimap::Hud);
	};

public:

	HUDMap* GetHUD() { return _hud; }
};

