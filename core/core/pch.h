#define DEBUG_MODE
#ifndef PCH_H
#define PCH_H

// add headers that you want to pre-compile here
#include "framework.h"

#include "Logger/Logger.h"
#ifdef DEBUG_MODE
#define print(msg, ...) Logger::Log(false, __FUNCTION__ ,msg, ##__VA_ARGS__)
inline char buffer[400];
#define fprint(msg, ...)  \
sprintf(buffer, msg, ##__VA_ARGS__);	\
std::cout << buffer << std::endl;


#else
#define print(msg, ...) 
#endif

typedef unsigned long long QWORD;
typedef unsigned long DWORD;

#define DEFINE_PADDING 0
#define STR_MERGE_IMPL(x, y)                x##y
#define STR_MERGE(x,y)                        STR_MERGE_IMPL(x,y)
#define MAKE_PAD(size)                        BYTE STR_MERGE(pad_, __COUNTER__) [ size ]
#define DEFINE_MEMBER_0(x)                    x;
#define DEFINE_MEMBER_N(x,offset)            struct { MAKE_PAD((QWORD)offset - DEFINE_PADDING); x; };
#define ReadQWORD(base, addr) *reinterpret_cast<QWORD*>((QWORD)base + (QWORD)addr)
#define ReadVTable(addr, idx) ((QWORD*)ReadQWORD(addr, 0))[(int)idx]



#include "Libraries/curl/curl.h"
#pragma comment(lib, "Libraries/curl/libcurl.lib")
#pragma comment(lib, "wldap32.lib" )
#pragma comment(lib, "Ws2_32.lib")
#pragma comment(lib, "crypt32.lib" )
#pragma comment(lib, "Normaliz.lib")
#include "Libraries/imgui/imgui.h"
#include "Libraries/imgui/imgui_impl_dx11.h"
#include "Libraries/imgui/imgui_impl_win32.h"
#include "Libraries/crt/crt.h"
#include "Memory/my_first_x64_bypass/Syscaller.h"
#include "Memory/Hooker/Whore.h"
#include "Memory/ret_spoof/ret_spoof.h"
#include "Memory/VMTHook/VMTHook.h"
#include "Memory/Memory/Memory.h"


#include "Offsets.h"


#include "SDK/Utility/Vector.h"
#include "SDK/Utility/Color.h"
#include "SDK/Utility/Hash.h"
#include "SDK/Utility/RiotString.h"

#include "SDK/DXDriver/DXDriver.h"
#include "SDK/DXDriver/MaterialRegistry.h"

#include "Renderer/Renderer.h"
#include "Renderer/Draw.h"

#include "GameHooks/HookManager/HookManager.h"

#include "SDK/Utility/Event.h"

#include "SDK/Pointer.h"

#include "SDK/GameClient/GameClient.h"

#include "SDK/RenderPipeline/MouseOverData.h"
#include "SDK/RenderPipeline/RenderPipeline.h"

#include "SDK/SpellData/SpellData.h"
#include "SDK/SpellInfo/SpellInfo.h"
#include "SDK/SpellCast/SpellCast.h"

#include "SDK/SpellBook/TargetingClient.h"
#include "SDK/SpellBook/SpellSlot.h"
#include "SDK/SpellBook/SpellBook.h"

#include "SDK/Navigation/Navigation.h"

#include "SDK/BuffManager/Script.h"
#include "SDK/BuffManager/Buff.h"
#include "SDK/BuffManager/BuffManager.h"

#include "SDK/Object/Object.h"
#include "SDK/Utility/ObjectTypeHolder.h"
#include "SDK/Object/AttackableUnit.h"
#include "SDK/Object/Turret.h"
#include "SDK/Utility/CharacterData.h"
#include "SDK/Object/Client.h"
#include "SDK/Object/Hero.h"

#include "SDK/ObjectManager/ObjectManager.h"

#include "SDK/Camera/Camera.h"
#include "SDK/HUD/HUD.h"
#include "SDK/Zoom/Zoom.h"
#include "SDK/Screen/Screen.h"

#include "SDK/TacticalMap/HUDMap.h"
#include "SDK/TacticalMap/TacticalMap.h"

#include "SDK/Engine/Engine.h"

#include "Scripts/TargetSelector/TargetSelector.h"
#include "Scripts/Orbwalker/Orbwalker.h"
#include "Scripts/Awareness/Icons.h"
#include "Scripts/Awareness/Awareness.h"

#endif //PCH_H
